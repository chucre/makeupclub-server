'use strict';

var expect = require('chai').expect,
    sinon  = require('sinon');

describe("TokenAuthentication", function(){
  var next,
      request,
      response,
      tokenAuthentication = require('../../midlewares/tokenAuthentication.js'),
      Token = require('../../models/token.js'),
      mongoose = require('mongoose-q')(require('mongoose')),
      Q = require('q'),
      account;

  beforeEach(function(){
    next = sinon.spy();
    request = { query: {}, body: {} };
    response = { json: sinon.spy() };
    sinon.stub(Token,'valid');
    account = {name: 'Fernando', email: 'fernano@example.com'}
  })

  afterEach(function(){
    Token.valid.restore();
  })

  it("should call next if has no token", function(){
    tokenAuthentication(request, require, next);
    expect(next.calledOnce).to.be.true;
    expect(request.account).to.be.undefined;
  });

  it("should call next if has a valid token", function(done){
    request.query.token="validOne";
    Token.valid.returns({execQ: function() { return Q.resolve({account: account}) }});
    tokenAuthentication(request, response, next).then(function(){
      expect(next.calledOnce).to.be.true;
      expect(request.account).to.be.equal(account);
      done();
    });
  });
  it("should not call next if has a invalid token", function(done){
    request.query.token="invalidOne";
    Token.valid.returns({execQ: function() { return Q.reject() }});
    tokenAuthentication(request, response, next).then(function(){
      expect(next.calledOnce).to.be.false;
      expect(request.account).to.be.undefined;
      expect(response.json.withArgs(401,{error: 'invalid token'}).calledOnce).to.be.true;
      done();
    });
  });
})
