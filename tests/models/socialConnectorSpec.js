'use strict';
var expect = require('chai').expect,
    sinon  = require('sinon');

describe("Social Connector", function(){
  var socialConnector = require('../../models/socialconnector.js'),
      FB = require('fb');

  describe("connect method", function(){
    before(function(){
      sinon.stub(socialConnector,'facebook');
    })

    after(function(){
      socialConnector.facebook.restore();
    })

    it('should call connector by name', function(){
      var network = {name: 'facebook'}
      socialConnector.connect(network);
      expect(socialConnector.facebook.withArgs(network).calledOnce).to.be.true
    });

    it('should reject if connector do not exist', function(done){
      var network = {name: 'networkThatNotExist'}
      socialConnector.connect(network).catch(function(error){
        expect(error).to.be.equal('network not exist');
        done();
      });
    });
  })

  describe("Facebook connector", function(done){
    before(function(){
      sinon.stub(FB,'api');
    })

    after(function(){
      FB.api.restore();
    })
    it("should reject if has no token", function(){
      var network = {name: 'facebook'}
      socialConnector.connect(network).catch(function(error){
        expect(error).to.be.equal('invalid facebook token');
        done();
      });
    });

    it("should return user account info", function(done){
      var network = {name: 'facebook', token: 'validOne'}
      var socialData = {uid: 'myid', name: 'my name', email: 'my@example.com'}
      socialConnector.connect(network).then(function(data){
        expect(data).to.be.equal(socialData);
        done();
      });
      FB.api.getCall(0).args[2](socialData);
    });
  })
})
