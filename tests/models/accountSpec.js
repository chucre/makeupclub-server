"use strict"

var expect    = require('chai').expect,
    sinon     = require('sinon'),
    mongoose  = require('mongoose'),
    Q         = require('q'),
    mongoUrl  = "mongodb://localhost:27017/tests";

describe("Account Model", function() {
  var reseller,
    account,
    mailerMock,
    Account;

  beforeEach(function(){
    mongoose.connect(mongoUrl);
    mailerMock = {
      send: sinon.spy()
    }

    Account = require('../../models/account.js')(mailerMock);

    reseller = new Account({
      email: 'reseller@example.com',
      location: 'Fortaleza/CE',
      want_be_resseler: true    
    })

    account = new Account({
      email: 'client@example.com',
      location: 'Fortaleza/CE'
    })

  })

  afterEach(function(){
    mongoose.connection.db.dropDatabase();
    mongoose.connection.close();
  });

  describe('after save',function() {

    var testEmail = function(args, done, view, options, debug) {
      try {
        
        expect(args[0]).to.be.equal(view);
        expect(args[1].from).to.be.equal(options.from);
        expect(args[1].to).to.be.equal(options.to);
        expect(args[1].account._id).to.be.equal(options.account._id);

      done();
      } catch(e){
      if (debug===true) {
        console.log(e);
        }
      }
    }

    it("should send mail to reseller",function(done){
      
      mailerMock.send = function() {
        
        testEmail(arguments, done, 'join_reseller',{
          from: 'Mirla Braga<mirla@makeupclubapp.com>',
          to: 'reseller@example.com',
          subject: 'Bem vinda Revendedora',
          account: reseller
        });
      
      }

      Account.createQ(reseller)
      .catch(done);

    })

    it("should send mail to costumer",function(done){
      
      mailerMock.send = function() {
        testEmail(arguments, done, 'join_customer',{
          from: 'Mirla Braga<mirla@makeupclubapp.com>',
          to: 'client@example.com',
          subject: 'Bem vinda',
          account: account
        });
      
      }

      Account.createQ(account)
      .catch(done);

    })


  })
})







