'use strict';

var expect   = require('chai').expect,
    sinon    = require('sinon'),
    mongoose = require('mongoose'),
    Q        = require('q'),
    mongoUrl = "mongodb://localhost:27017/tests",
    Brand    = require('../../models/brand.js'),
    Post     = require('../../models/post.js');


describe("Buy Model", function() {
  var reseller,
      account,
      account2,
      account3,
      post,
      buy,
      brand,
      mailerMock,
      Buy,
      Account;

  beforeEach(function(){
    mongoose.connect(mongoUrl);
  })

  afterEach(function(){
    mongoose.connection.db.dropDatabase();
    mongoose.connection.close();
  });

  beforeEach(function(done){
    mailerMock = {
      send: sinon.spy()
    }

    Account = require('../../models/account.js')(mailerMock);
    Buy = require('../../models/buy.js')(mailerMock);
    brand = new Brand();
    reseller = new Account({
      brand: brand,
      email: 'reseller@example.com',
      location: 'Fortaleza/CE',
      reseller: true,
      brands: [brand]
    })
    account = new Account({
      invited_by: [reseller],
      email: 'client@example.com',
      location: 'Fortaleza/CE'
    })
    account2 = new Account({
      email: 'client2@example.com',
      location: 'Fortaleza/CE'
    })
    account3 = new Account({
      email: 'client3@example.com',
      location: 'Fortaleza/CE',
      invited_by: [account2],
    })
    post = new Post({
      name : 'post'
    })
    buy = {
      brand: brand,
      account: account,
      post: post
    }
    Q.all([
      brand.saveQ(),
      reseller.saveQ(),
      account.saveQ(),
      account2.saveQ(),
      account3.saveQ(),
      post.saveQ()
    ])
    .then(function(){
      done();
    })
    .catch(done);
  })

  describe("after save", function(){

    var testEmail = function(args, done, view, options, debug) {
      try {
        expect(args[0]).to.be.equal(view);
        expect(args[1].from).to.be.equal(options.from);
        expect(args[1].to).to.be.equal(options.to);
        expect(args[1].subject).to.be.equal(options.subject);
        expect(args[1].post._id.toString()).to.be.equal(options.post._id.toString());
        expect(args[1].account._id.toString()).to.be.equal(options.account._id.toString());
        expect(args[1].reseller._id.toString()).to.be.equal(options.reseller._id.toString());
        done();
      } catch(e){
        if (debug===true) {
          console.log(e);
        }
      }
    }

    it("should send email to customer", function(done){
      mailerMock.send = function() {
        testEmail(arguments, done, 'buy_customer_with_reseller',{
          from: 'Mirla Braga<mirla@makeupclubapp.com>',
          to: 'client2@example.com',
          subject: 'Uma revendedora entrará em contato em breve',
          post: post,
          account: account2,
          reseller: reseller
        });
      }
      buy.account = account2;
      Buy.createQ(buy)
      .catch(done)
    })
    describe("should send email to reseller", function(){
      it("to known customer", function(done){
        mailerMock.send = function() {
          testEmail(arguments, done, 'buy_reseller_known_customer',{
            from: 'Mirla Braga<mirla@makeupclubapp.com>',
            to: 'reseller@example.com',
            subject: 'Você tem uma nova visita para agendar',
            post: post,
            account: account,
            reseller: reseller
          });
        }
        Buy.createQ(buy)
        .catch(done)
      })
      it("to unknown customer", function(done){
        mailerMock.send = function() {
          testEmail(arguments, done, 'buy_reseller_unknown_customer',{
            from: 'Mirla Braga<mirla@makeupclubapp.com>',
            to: 'reseller@example.com',
            subject: 'Você tem uma nova cliente para visitar',
            post: post,
            account: account2,
            reseller: reseller
          });
        }
        buy.account = account2;
        Buy.createQ(buy)
        .catch(done)
      })
    });
  })

  describe("before save", function(){
    it("should chose reseller that invite client", function(done){
      Buy.createQ(buy)
      .then(function(buy){
        expect(buy.reseller.toString()).to.be.equal(reseller._id.toString());
        done();
      })
      .catch(done)
    })

    it("should chose random reseller if client join without reseller", function(done){
      buy.account = account2;
      Buy.createQ(buy)
      .then(function(buy){
        expect(buy.reseller.toString()).to.be.equal(reseller._id.toString());
        done();
      })
      .catch(done);
    })
    it("should chose random reseller if client is invited by normal account", function(done){
      buy.account = account3;
      Buy.createQ(buy)
      .then(function(buy){
        expect(buy.reseller.toString()).to.be.equal(reseller._id.toString());
        done();
      })
      .catch(done);
    });
  })
});
