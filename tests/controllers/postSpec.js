'use strict';

var expect = require('chai').expect,
    sinon  = require('sinon'),
    Q      = require('q'),
    expressValidator = require('express-validator');

describe("Post Controller", function(){

  var PostController = require('../../controllers/post/index.js'),
      Post = require('../../models/post.js'),
      controller,
      routerMock,
      responseMock,
      requestStub;

  beforeEach(function(){
    routerMock = {
      get: sinon.spy(),
      post: sinon.spy()
    }
    responseMock = {
      json: sinon.spy()
    }

    controller = PostController(routerMock);

    requestStub = {
      body: {},
      query: {}
    }

    expressValidator()(requestStub,null,sinon.spy());
  })


  describe("routes", function(){
    it("should route /post [GET] to index", function(){
      expect(routerMock.get.withArgs('/',controller.index).calledOnce).to.be.true;
    })
  })

  describe("index method", function(){

    var posts;

    beforeEach(function(){
      sinon.stub(Post,'getRecents')
    })

    afterEach(function(){
      Post.getRecents.restore();
    })

    it("should request authentication", function(){
      controller.index(requestStub, responseMock);
      expect(responseMock.json.withArgs(400, {error: 'invalid credentials'})
          .calledOnce).to.be.true;
    });

    it("should return last 5 posts", function(done){
      posts = [{id:1},{id:2},{id:3},{id:4},{id:5}]
      requestStub.account = {id: 'myID'}
      Post.getRecents.withArgs().returns({ execQ: function() {return Q.resolve(posts) }});
      controller.index(requestStub, responseMock).then(function(){
        expect(responseMock.json.withArgs(200, posts)
          .calledOnce).to.be.true;
        done();
      })
    });

    it("should accept lastPost param and return the next 5", function(done){
      posts = [{id:6}]
      requestStub.account = {id: 'myID'}
      requestStub.query.lastPost = 5;

      Post.getRecents.withArgs(5).returns({ execQ: function() {return Q.resolve(posts) }});
      controller.index(requestStub, responseMock).then(function(){
        expect(responseMock.json.withArgs(200, posts)
          .calledOnce).to.be.true;
        done();
      })
    });

  })
});
