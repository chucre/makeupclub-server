'use strict';


var expect = require('chai').expect,
    sinon  = require('sinon'),
    Q      = require('q'),
    expressValidator = require('express-validator');

describe("Brand Controller", function() {

  var BrandsController = require('../../controllers/brands.js'),
      Brand = require('../../models/brand.js'),
      controller,
      routerMock,
      responseMock,
      requestStub;

  beforeEach(function(){
    routerMock = {
      get: sinon.spy(),
      post: sinon.spy()
    }
    responseMock = {
      json: sinon.spy()
    }
    requestStub = {
      body: {},
      params: {}
    }
    controller = BrandsController(routerMock)
  })

  beforeEach(function(){
    sinon.stub(Brand,'fromLocation');
    sinon.stub(Brand,'find');
  })

  afterEach(function(){
    Brand.fromLocation.restore();
    Brand.find.restore();
  })

  it("should route / [GET]", function(){
    expect(routerMock.get.withArgs('/',controller.brands).calledOnce).to.be.true;
  })

  it("should request authentication", function(){
    controller.brands(requestStub, responseMock);
    expect(responseMock.json.withArgs(400, {error: 'invalid credentials'})
        .calledOnce).to.be.true;
  });

  xit("should return 200 if success and return brands that have reseller from same location of client", function(done){
    requestStub.account = {
      location: 'fortaleza'
    }
    var brands = ['mk','avon','natura']
    Brand.fromLocation.withArgs('fortaleza').returns({ execQ: function() {return Q.resolve(brands) }});
    controller.brands(requestStub, responseMock).then(function(){
      expect(responseMock.json.withArgs(200, brands)
          .calledOnce).to.be.true;
      done();
    });
  });

  it("should return 200 if success and return brands", function(done){
    requestStub.account = {
      location: 'fortaleza'
    }
    var brands = ['mk','avon','natura']
    Brand.find.returns({ execQ: function() {return Q.resolve(brands) }});
    controller.brands(requestStub, responseMock).then(function(){
      expect(responseMock.json.withArgs(200, brands)
          .calledOnce).to.be.true;
      done();
    });
  });
});
