'use strict';

var expect = require('chai').expect,
    sinon  = require('sinon'),
    Q      = require('q'),
    expressValidator = require('express-validator');

describe("Buy Controller", function() {

  var BuyController = require('../../controllers/buy/index.js'),
    Brand = require('../../models/brand.js'),
    Post = require('../../models/post.js'),
    Buy,
    mailerMock,
    controller,
    routerMock,
    responseMock,
    requestStub;

  beforeEach(function(){
    routerMock = {
      get: sinon.spy(),
      post: sinon.spy()
    }
    responseMock = {
      json: sinon.spy()
    }
    requestStub = {
      body: {},
      params: {}
    }
    mailerMock = {
      send: sinon.spy()
    }
    Buy = require('../../models/buy.js')(mailerMock);
    controller = BuyController(routerMock)
  })

  beforeEach(function(){
    sinon.stub(Brand,'find')
    sinon.stub(Post,'find')
    sinon.stub(Buy,'createQ')
  })

  afterEach(function(){
    Brand.find.restore();
    Post.find.restore();
    Buy.createQ.restore();
  })

  it("should route / [POST]", function(){
    expect(routerMock.post.withArgs('/',controller.buy).calledOnce).to.be.true;
  })

  it("should request authentication", function(){
    controller.buy(requestStub, responseMock);
    expect(responseMock.json.withArgs(400, {error: 'invalid credentials'})
        .calledOnce).to.be.true;
  });

  it("should return 400 if brand are invalid", function(done){
    requestStub.account = {location: 'fortaleza'}
    requestStub.body    = {
      brand_id: 'brand_id'
    }
    var erro = {error: 'invalid brand'};

    Brand.find.withArgs({_id: 'brand_id'}).returns({ execQ: function() {return Q.reject([]) }});

    controller.buy(requestStub, responseMock).then(function(){
      expect(responseMock.json.withArgs(400, erro)
          .calledOnce).to.be.true;
      done();
    });
  });

  it("should return 400 if post are invalid", function(done){
    requestStub.account = {location: 'fortaleza'}
    requestStub.body    = {
      post_id: 'post_id',
      brand_id: 'brand_id'
    }
    var erro = {error: 'invalid post'};

    Brand.find.withArgs({_id: 'brand_id'}).returns({ execQ: function() {return Q.resolve([]) }});
    Post.find.withArgs({_id: 'post_id'}).returns({ execQ: function() {return Q.reject([]) }});


    controller.buy(requestStub, responseMock).then(function(){
      expect(responseMock.json.withArgs(400, erro)
          .calledOnce).to.be.true;
      done();
    });
  });

  it("should return 200 if success",function(done){
    var account = {location: 'fortaleza'};
    requestStub.account = account;
    requestStub.body    = {
      brand_id: 'brand_id',
      post_id: 'post_id'
    }

    var brand_mk = 'mk';
    var post = 'post';
    var buy = {brand: 'brand_id', account: account, post: 'post_id'};

    Brand.find.withArgs({_id: 'brand_id'}).returns({ execQ: function() {return Q.resolve(brand_mk) }});
    Post.find.withArgs({_id: 'post_id'}).returns({ execQ: function() {return Q.resolve(post) }});

    Buy.createQ.withArgs(buy).returns(Q.resolve(buy));
    controller.buy(requestStub, responseMock).then(function(){
      expect(responseMock.json.withArgs(200, buy)
          .calledOnce).to.be.true;
      done();
    });
  })
});
