var expect = require('chai').expect,
  sinon = require('sinon'),
  Q = require('q'),
  expressValidator = require('express-validator');

describe("Session Controller", function() {

  var SessionController = require('../../controllers/session/index.js'),
    Token = require('../../models/token.js'),
    Account,
    SocialConnector = require('../../models/socialconnector.js'),
    controller,
    routerMock,
    responseMock,
    requestStub,
    networkData,
    mailerMock;

  beforeEach(function() {
    routerMock = {
      get: sinon.spy(),
      post: sinon.spy()
    }
    responseMock = {
      json: sinon.spy()
    }
    controller = SessionController(routerMock);
    networkData = {
      name: 'facebook',
      token: 'validOne'
    }
    requestStub = {
      body: {
        network: networkData
      },
      params: {}
    }

    mailerMock = {
      send: sinon.spy()
    }

    Account = require('../../models/account.js')(mailerMock);

    expressValidator()(requestStub, null, sinon.spy());
  })

  beforeEach(function() {
    sinon.stub(SocialConnector, 'connect');
    sinon.stub(Account, 'findOne');
    sinon.stub(Token, 'createNew');
  })

  afterEach(function() {
    SocialConnector.connect.restore();
    Account.findOne.restore();
    Token.createNew.restore();
  })

  it("should route / [POST]", function() {
    expect(routerMock.post.withArgs('/', controller.create).calledOnce).to.be.true;
  });

  it("should return 401 if network is invalid", function(done) {
    SocialConnector.connect.withArgs(requestStub.body.network)
      .returns(Q.reject('invalid network credentials'));
    controller.create(requestStub, responseMock)
      .then(function() {
        expect(responseMock.json.withArgs(401, {
          error: 'invalid network credentials'
        }).calledOnce).to.be.true;
        done();
      });
  })

  it("should return 401 if no account was found", function(done) {
    SocialConnector.connect.withArgs(requestStub.body.network)
      .returns(Q.resolve({
        id: 'myId'
      }));

    Account.findOne.withArgs({
      'facebook.id': 'myId'
    })
      .returns({
        execQ: function() {
          return Q.reject('account not found')
        }
      })

    controller.create(requestStub, responseMock)
      .then(function() {
        expect(responseMock.json.withArgs(401, {
          error: 'account not found'
        }).calledOnce).to.be.true;
        done();
      });
  })

  it("should return 401 if no token was created", function(done) {
    var account = {
      id: 'id'
    };

    SocialConnector.connect.withArgs(requestStub.body.network)
      .returns(Q.resolve({
        id: 'myId'
      }));

    Account.findOne.withArgs({
      'facebook.id': 'myId'
    })
      .returns({
        execQ: function() {
          return Q.resolve({
            id: 'id'
          })
        }
      });

    Token.createNew.withArgs(account).returns(Q.reject('some error'));

    controller.create(requestStub, responseMock)
      .then(function() {
        expect(responseMock.json.withArgs(401, {
          error: 'some error'
        }).calledOnce).to.be.true;
        done();
      });
  })
  it("should return 200 if create a new Token", function(done){
    var account = {
      id: 'id'
    };

    var token = {token: "myToken", id:'tokenID', validUntil: 'validUntil tomorow'};

    SocialConnector.connect.withArgs(requestStub.body.network)
      .returns(Q.resolve({
        id: 'myId'
      }));

    Account.findOne.withArgs({
      'facebook.id': 'myId'
    })
      .returns({
        execQ: function() {
          return Q.resolve({
            id: 'id'
          })
        }
      });

    Token.createNew.withArgs(account).returns(Q.resolve(token));

    controller.create(requestStub, responseMock)
      .then(function() {
        expect(responseMock.json.withArgs(200, {token: token.token, validUntil: token.validUntil}).calledOnce).to.be.true;
        done();
      });
  });
})
