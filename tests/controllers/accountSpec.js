'use strict';

var expect = require('chai').expect,
    sinon  = require('sinon'),
    Q      = require('q'),
    expressValidator = require('express-validator');

var error = function(param, msg) {
  return [{param: param, msg: msg, value: undefined}]
}

describe("Account Controller", function() {

  var AccountController = require('../../controllers/account/index.js'),
      SocialConnector = require('../../models/socialconnector.js'),
      controller,
      routerMock,
      responseMock,
      requestStub,
      errors,
      accountData,
      Account,
      mailerMock;

  beforeEach(function(){
    routerMock = {
      get: sinon.spy(),
      post: sinon.spy()
    }
    mailerMock = {
      send: sinon.spy()
    }
    Account = require('../../models/account.js')(mailerMock),
    responseMock = {
      json: sinon.spy()
    }
    controller = AccountController(routerMock);
    accountData = {
      name: 'fernando',
      email: 'fernando@example.com',
      network: {
        name: 'facebook',
        token: 'myDummyToken'
      },
      want_be_resseler: true
    }
    requestStub = {
      body: accountData,
      params: {}
    }

    expressValidator()(requestStub,null,sinon.spy());
  })

  it('should router / [GET]', function(){
    expect(routerMock.get.withArgs('/',controller.me).calledOnce).to.be.true;
  })

  it('should router /:id [GET]', function(){
    expect(routerMock.get.withArgs('/:id',controller.info).calledOnce).to.be.true;
  })

  it('should router / [POST]', function(){
    expect(routerMock.post.withArgs('/',controller.join).calledOnce).to.be.true;
  })

  describe("join method", function(){

    var createDeferred,
        facebookDeferred;

    beforeEach(function(){
      createDeferred = Q.defer();
      sinon.stub(Account,'createQ').returns(createDeferred.promise);

      facebookDeferred = Q.defer();
      sinon.stub(SocialConnector,'connect').returns(facebookDeferred.promise);
    })

    afterEach(function(){
      Account.createQ.restore();
      SocialConnector.connect.restore();
    })

    describe("validations", function(){

      afterEach(function(){
        controller.join(requestStub, responseMock);
        expect(responseMock.json.withArgs(400, {errors: errors}).calledOnce).to.be.true;
      })

      it("should check name", function(){
        delete requestStub.body.name;
        errors = error('name','Invalid value');
      })

      it("should check email", function(){
        delete requestStub.body.email;
        errors = error('email','Invalid value');
      });

      it("should check network name", function(){
        delete requestStub.body.network.name;
        errors = error('network.name','Invalid value');
      });

      it("should check network token", function(){
        delete requestStub.body.network.token;
        errors = error('network.token','Invalid value');
      })

    })

    it("should return 200 with success", function(done){
      var data = {
        name: accountData.name,
        email: accountData.email,
        location: accountData.location,
        phone: accountData.phone,
        facebook: 'socialData',
        want_be_resseler: true
      }
      createDeferred.resolve(data);
      facebookDeferred.resolve('socialData');
      controller.join(requestStub, responseMock).then(function(){
        expect(Account.createQ.withArgs(data).calledOnce).to.be.true;
        expect(responseMock.json.withArgs(200, data).calledOnce).to.be.true;
        done();
      })
      .catch(function(err){
        done(err)
      });
    });

    it("should return 400 with fail", function(done){
      facebookDeferred.resolve('social');
      createDeferred.reject('err');
      controller.join(requestStub, responseMock).then(function(res){
        done(res);
      }).catch(function(err){
        expect(responseMock.json.withArgs(400, sinon.match.any).calledOnce).to.be.true;
        done();
      });
    });

    it("should return 400 with social fail", function(done){
      facebookDeferred.reject('social');
      controller.join(requestStub, responseMock).then(function(res){
        done(res);
      }).catch(function(err){
        expect(responseMock.json.withArgs(400, sinon.match.any).calledOnce).to.be.true;
        done();
      });
    });
  });

  describe("info method", function(){

    before(function() {
      sinon.stub(Account, "findOne")
    })

    after(function() {
      Account.findOne.restore();
    })

    it("should return 200 with success", function(done) {
      requestStub.params.id = "MY_ID";
      Account.findOne.withArgs({_id: requestStub.params.id})
        .returns({execQ: function() { return Q.resolve(accountData) }})
      controller.info(requestStub, responseMock)
        .then(function() {
          expect(responseMock.json.withArgs(200, accountData).calledOnce).to.be.true;
          done();
        })
    });

    it("should return 404 if not found", function(done) {
      requestStub.params.id = "MY_OTHER_ID";
      Account.findOne.withArgs({_id: requestStub.params.id})
        .returns({execQ: function() { return Q.reject("error") }})
      controller.info(requestStub, responseMock)
        .then(function() {
          expect(responseMock.json.withArgs(404, {}).calledOnce).to.be.true;
          done();
        })
    });

  });

  describe("me method", function(done){
    it("should return 200 with success", function() {
      requestStub.account = accountData;
      controller.me(requestStub, responseMock)
      .then(function(argument) {
        expect(responseMock.json.withArgs(200, accountData).calledOnce).to.be.true;
        done();
      })
      .catch(done);
    });

    it("should return 400 if has no account", function() {
      requestStub.account = undefined;
      controller.me(requestStub, responseMock);
      expect(responseMock.json.withArgs(400, {error: "request has no account"}).calledOnce).to.be.true;
    });
  })
});
