'use strict';

describe("Invite Controller", function() {
    it("should route / [POST]")
    it("should return 401 if friends are invalid")
    it("should return 401 if name of friend are invalid")
    it("should return 401 if email and phone of friend are invalid")
    it("should return 200 friends are valid")
    it("should send email to friend")
    it("should send sms to friend")
});
