'use strict';
var FB = require('fb'),
    Q  = require('q');

var SocialConnector = {
  facebook: function(params) {
    if (!params.token) {
      return Q.reject('invalid facebook token')
    } else {
      var defer = Q.defer();
      FB.api('/me', { fields: ['id', 'name', 'email', 'birthday', 'location'], access_token: params.token }, function (res) {
        if(!res || res.error) {
          var error = (res)?res.error:'facebook connection error';
          defer.reject(error);
        } else {
          defer.resolve(res);
        }
      });
      return defer.promise;
    }
  },
  connect : function(network) {
    if(this[network.name]) {
      return this[network.name](network);
    } else {
      return Q.reject('network not exist');
    }
  },
  friends : function(token) {
    if (!token) {
      return Q.reject('invalid facebook token')
    } else {
      var defer = Q.defer();
      FB.api('/me/friends', { fields: ['id', 'name', 'email', 'birthday', 'location'], access_token: token }, function (res) {
        if(!res || res.error) {
          var error = (res)?res.error:'facebook connection error';
          defer.reject(error);
        } else {
          defer.resolve(res.data);
        }
      });
      return defer.promise;
    }
  }
}

module.exports = SocialConnector;
