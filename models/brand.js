'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema;

var brandSchema = new Schema({
  name: String,
  image_url: String,
  cities: [String]
});

brandSchema.statics.fromLocation = function(location){
  return this.find({ cities: location });
}

var Brand = mongoose.model('Brand',
  brandSchema
);


module.exports = Brand;
