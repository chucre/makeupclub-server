'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema,
    timestamp = require('mongoose-timestamp'),
    removedTimestamp = require('mongoose-paranoid_remove');

var taskSchema = new Schema({
  client: { type: Schema.Types.ObjectId, ref: 'Client'},
  start_at: Date,
  duration: String,
  type: String,
  is_realized: Boolean,
  account: { type: Schema.Types.ObjectId, ref: 'Account' },
  sale: { type: Schema.Types.ObjectId, ref: 'Sale' },
  sale_activity_type: String
});
taskSchema.plugin(timestamp);
taskSchema.plugin(removedTimestamp);
var Task = mongoose.model('Task',
  taskSchema
);

module.exports = Task;
