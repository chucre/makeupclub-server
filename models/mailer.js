'use strict';

var jade = require('jade'),
    mailer = require('nodemailer'),
    juice = require('juice2'),
    Q = require('q');

var transporter = mailer.createTransport({
    service: 'Mandrill',
    auth: {
        user: 'fernando@makeupclubapp.com',
        pass: 'lnuw6xoWJF6KxY4VcSOm1Q'
    }
  });

module.exports = {
  send: function(viewName, options) {
    try {
      var deferred = Q.defer();
      var view = jade.compileFile(__dirname+'/../views/'+viewName+'.jade', {});
      var body = view(options);
      juice.juiceContent(body, { url: 'file://'}, function(err, html){
        if (err) {
          deferred.reject(err);
          console.log(err)
          return;
        }
        var mailOptions = {
          from: options.from,
          to: options.to,
          subject: options.subject,
          text: options.alt,
          html: html
        };

        transporter.sendMail(mailOptions, function(error,info){
          if (error) {
            deferred.reject(error);
            console.log(error);
          } else {
            deferred.resolve(info);
          }
        });
      });
      return deferred.promise;
    } catch(e) {
      console.log(e);
      throw e;
    }
  }
};
