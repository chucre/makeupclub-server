var Client = require('node-rest-client').Client,
    Q = require('q');

module.exports = function(time, title, data, devices) {
  var request = {
    data: 
    {
      request: {
        application  : "D541E-E9ACA",
        auth : "bGxITHUFy0kiOtlFxbeEBnVVICYW0XVbj0feNz0ewkTqJfI8RzBoWQrpc8zcBODcL1pYsp6u6UZPwl8OFKig",
        notifications : [{
          send_date  : time,
          content  : title,
          data : data
        }],
        "android_header":"header"
      }
    },
    headers:{"Content-Type": "application/json"}
  },
  client = new Client(),
  defer = Q.defer();

  if (devices) {
    request.data.request.notifications[0].devices = devices;
  } else {
    return defer.reject('we need devices');
  }

  client.post("https://cp.pushwoosh.com/json/1.3/createMessage", request, function(data,response) {
    if (response.statusCode == 200) {
      defer.resolve(data);
    } else {
      defer.reject(data);
    }
  });

  return defer.promise;
}