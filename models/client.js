'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema,
    timestamp = require('mongoose-timestamp'),
    removedTimestamp = require('mongoose-paranoid_remove');

var clientSchema = new Schema({
  name: String,
  email: String,
  phone: String,
  phones: [String],
  tags: [String],
  photo_profile: String,
  is_whatsapp: Boolean,
  account: { type: Schema.Types.ObjectId, ref: 'Account' }
});
clientSchema.plugin(timestamp);
clientSchema.plugin(removedTimestamp);
var Client = mongoose.model('Client',
  clientSchema
);

module.exports = Client;
