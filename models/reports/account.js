var Q = require('q'),
    moment = require('moment');

var dateFormat = function(date) {
  return [date.getDate(), date.getMonth()+1, date.getFullYear()].join('/');
}

function clientsByDay(start, end) {
  try {
    var currentDay = new Date(start);
    var endDay = new Date(end);
    return this.clients()
    .where({createdAt:{$gte: currentDay, $lte: endDay}, deletedAt:{$exists: false}})
    .execQ()
    .then(function(clients){
      var result = {};
      clients.forEach(function(client){
        var date = dateFormat(client.createdAt);
        result[date] = result[date] || {
          date: client.createdAt,
          quantity: 0
        };
        result[date].quantity++;
      });
      while(currentDay <= endDay) {
        var date = dateFormat(currentDay);
        result[date] = result[date] || {
          date: currentDay,
          quantity: 0
        };
        currentDay = new Date(currentDay);
        currentDay.setDate(currentDay.getDate()+1);
      }
      var data = [];
      for(var i in result) {
        data.push({
          period: i,
          date: result[i].date,
          quantity: result[i].quantity
        });
      }
      data.sort(function(a,b){
        return new Date(a.date) - new Date(b.date);
      })
      var finalresult = {
        data: data
      };
      return finalresult;
    })
    .catch(function(err){
      console.log(err.toString());
    });
  } catch(err) {
    console.log(err.toString());
  }
}

function salesByDay(start, end) {
  try {
    var currentDay = new Date(start);
    var endDay = new Date(end);
    return this.sales()
    .where({date_sale:{$gte: currentDay, $lte: endDay}, deletedAt: {$exists: false}})
    .populate('items.product')
    .execQ()
    .then(function(sales){
      var lines = {};
      var result = sales.reduce(function(result, sale){
        var itemResult = result[dateFormat(sale.date_sale)] || {
          period: dateFormat(sale.date_sale),
          date: sale.date_sale
        };
        sale.items.reduce(function(itemResult, item){
          var line = item.product.line;
          
          itemResult[line] = itemResult[line] || 0;
          itemResult[line] += item.quantity* item.price;
          
          lines[line] = lines[line] || 0;
          lines[line] += itemResult[line];
          
          return itemResult;
        }, itemResult);
        
        result[itemResult.period] = itemResult;
        
        return result;
      },{});
      while(currentDay <= endDay) {
        var item = result[dateFormat(currentDay)] || {
          period: dateFormat(currentDay),
          date: currentDay
        };
        for(var line in lines) {
          item[line] = item[line]||0;
        }
        result[item.period] = item;
        currentDay = new Date(currentDay);
        currentDay.setDate(currentDay.getDate()+1);
      }
      var data = [];
      for(var i in result) {
        data.push(result[i]);
      }
      data.sort(function(a,b){
        return new Date(a.date) - new Date(b.date);
      })
      var finalresult = {
        data: data, 
        lines: [],
        totalLines: []
      };
      for (var line in lines) {
        finalresult.lines.push(line);
        finalresult.totalLines.push({
          label: line,
          value: lines[line]
        })
      }
      return finalresult;
    })
    .catch(function(err){
      console.log(err.toString());
    });
  } catch(err) {
    console.log(err.toString());
  }
}

function tasksByDay(start, end) {
  try {
    var currentDay = new Date(start);
    var endDay = new Date(end);
    return this.tasks()
    .where({start_at:{$gte: currentDay, $lte: endDay}, deletedAt: {$exists: false}})
    .execQ()
    .then(function(tasks){
      var types = {};
      var result = tasks.reduce(function(result, task){
        var item = result[dateFormat(task.start_at)] || {
          period: dateFormat(task.start_at),
          date: task.start_at,
        };
        types[task.type] = types[task.type]||0;
        types[task.type]++;

        item[task.type] = item[task.type] || 0;
        item[task.type]++;
        result[item.period] = item;
        return result;
      },{});
      while(currentDay <= endDay) {
        var item = result[dateFormat(currentDay)] || {
          period: dateFormat(currentDay),
          date: currentDay
        };
        for(var type in types) {
          item[type] = item[type]||0;
        }
        result[item.period] = item;
        currentDay = new Date(currentDay);
        currentDay.setDate(currentDay.getDate()+1);
      }
      var data = [];
      for(var i in result) {
        data.push(result[i]);
      }
      data.sort(function(a,b){
        return new Date(a.date) - new Date(b.date);
      })
      var finalresult = {
        data: data, 
        types: [],
      };
      for (var type in types) {
        finalresult.types.push(type);
      }
      return finalresult;
    })
    .catch(function(err){
      console.log(err.toString());
    });
  } catch(err) {
    console.log(err.toString());
  }
}

var salesReport = function() {
  var defer = Q.defer();
  this.sales().execQ()
  .then(function(sales){
    var start_week = new Date();
    start_week.setHours(0);
    start_week.setMinutes(0);
    start_week.setSeconds(0);
    start_week.setMilliseconds(0);

    var start_month = new Date(start_week.getTime());

    start_week.setDate(start_week.getDate()-start_week.getUTCDay());
    start_month.setDate(1);

    var report = sales.reduce(function(data, sale){
      data.total = data.total+sale.total_sale_value;
      if (sale.date_sale >= start_week) {
        data.week = data.week+sale.total_sale_value;
      }
      if (sale.date_sale >= start_month) {
        data.month = data.month+sale.total_sale_value;
      }
      return data;
    }, {total: 0, month: 0, week: 0});
    defer.resolve(report);
  });
  return defer.promise;
}

module.exports = function(accountSchema){
  accountSchema.methods.salesByDay = salesByDay;
  accountSchema.methods.clientsByDay = clientsByDay;
  accountSchema.methods.tasksByDay = tasksByDay;
  accountSchema.methods.salesReport = salesReport;
}
