var Client = require('../client.js')
    moment = require('moment'),
    Q      = require('q');

function clientsByDay(start, end) {
  try {
    var deferred = Q.defer();
    Client
    .aggregate({
      $match:{
        createdAt: {$gte: start, $lt: end},
        account: {$in: this.supervisedAccounts}
      }
    })
    .group({
      _id : "_id",
      date: { 
        day: { $dayOfMonth: "$createdAt" }, 
        month: { $month: "$createdAt" }, 
        year: { $year: "$createdAt" }
      },
      count: { $sum: 1 }
    })
    .exec(function(err, totals){
      if (err) {
        console.log(err.toString());
        deferred.reject(err);
        return;
      }
      console.log(totals);
      var current = moment(start),
          result = {
            data: [],
            accounts: []
          }
      while(current.isBefore(end)) {
        
      }
      deferred.resolve(result);
    },function(err){
      console.log(err.toString());
    });
  } catch(err) {
    console.log(err);
  }
  return deferred.promise;
}

module.exports = function(accountSchema){
  accountSchema.methods.supervisorClientsByDay = clientsByDay;
}
