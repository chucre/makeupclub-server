'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema,
    timestamp = require('mongoose-timestamp'),
    removedTimestamp = require('mongoose-paranoid_remove');;

var productSchema = new Schema({
	"code": String,
	"name":String,
	"url": String,
	"price": Number,
	"original_price":Number,
	"line": String,
	"cover_image_url": {
		"thumb":String,
		"medium":String,
		"large":String
	},
	"catalog_name":String,
  "description":String,
  "benefits":String,
	"brand":{ type: Schema.Types.ObjectId, ref: 'Brand' }
});


productSchema.plugin(timestamp);
productSchema.plugin(removedTimestamp);

var Product = mongoose.model('Product',
  productSchema
);

module.exports = Product;
