'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema,
    timestamp = require('mongoose-timestamp'),
    removedTimestamp = require('mongoose-paranoid_remove');

var inviteSchema = new Schema({
  name: String,
  email: String,
  is_managed: Boolean,
  account: { type: Schema.Types.ObjectId, ref: 'Account' }
});

inviteSchema.plugin(timestamp);
inviteSchema.plugin(removedTimestamp);
var Invite = mongoose.model('Invite',
  inviteSchema
);

module.exports = Invite;
