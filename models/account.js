'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs'),
    Q     = require('q'),
    timestamp = require('mongoose-timestamp'),
    removedTimestamp = require('mongoose-paranoid_remove'),
    Task = require('./task.js'),
    Client = require('./client.js'),
    Sale = require('./sale.js'),
    Invite = require('./invite.js'),
    AccountReport = require('./reports/account.js'),
    Account;

var accountSchema = new Schema({
  name: String,
  address: String,
  thumb: String,
  email: { type: String },
  location: String,
  phone: String,
  password: String,
  password_reset_token: String,
  facebook: { type: Schema.Types.Mixed },
  invited_by: [{ type: Schema.Types.ObjectId, ref: 'Account' }],
  reseller: Boolean,
  brands: [{ type: Schema.Types.ObjectId, ref: 'Brand' }],
  plan: { type: Schema.Types.ObjectId, ref: 'Plan' },
  validUntil: Date,
  transactions: [{ type: Schema.Types.Mixed }],
  lastDraw: Date,
  want_be_resseler: Boolean,
  devices: [String],
  apps: [String],
  posts_credits: Number,
  mailer:{
    app_install: Boolean,
    without_sales_warning: Date,
    without_tasks_warning: Date,
  },
  points: [{
    points: Number,
    'for': String,
    date: Date,
    data: { type: Schema.Types.Mixed }
  }],
  isSupervisor: Boolean,
  supervisedAccounts: [{ type: Schema.Types.ObjectId, ref: 'Account' }],
  supervisor: { type: Schema.Types.ObjectId, ref: 'Account' },
  permissions: { type: Schema.Types.Mixed }
});

AccountReport(accountSchema);
accountSchema.plugin(timestamp);
accountSchema.plugin(removedTimestamp);

accountSchema.statics.points = {
  SCHEDULE: 1,
  PROMO_CODE: 1,
  PROMO_USED: 1,
  INVITE: 2,
  INVITE_JOINED: 8
}

accountSchema.statics.addPoint = function(accountId, points, _for, data){
  return Account.findOne({_id: accountId}).execQ()
  .then(function(account){
    account.points.push({points: points, 'for': _for, data: data, date: new Date()});
    return account.saveQ();
  });
}

accountSchema.methods.generateHash = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

accountSchema.methods.tasks = function() {
  return Task.where({account: this, deletedAt:{$exists: false}});
}

accountSchema.methods.clients = function() {
  return Client.where({account: this, deletedAt:{$exists: false}});
}

accountSchema.methods.sales = function() {
  return Sale.where({account: this, deletedAt:{$exists: false}});
}

accountSchema.methods.invites = function() {
  return Invite.where({account: this, deletedAt:{$exists: false}});
}

accountSchema.methods.generateResetHash = function() {
  var buf = new Buffer(64);
  for (var i = 0; i < buf.length; i++) {
      buf[i] = Math.floor(Math.random() * 256);
  }
  var id = buf.toString('base64');
  this.password_reset_token = id;
}

// checking if password is valid
accountSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

var sendEmailWhenRegister = function(){
  var account = this,
      view = 'join_customer',
      options = {
        from: 'Mirla Braga<mirla@makeupclubapp.com>',
        to: account.email,
        subject: 'Bem vinda!',
        account: account
      };
  if (account.apps.indexOf('makeupclubapp') == -1) {
    return;
  }

  if(!account.want_be_resseler){
      accountSchema.mailer.send(view,options);
    } else if (true === account.want_be_resseler) {
      view = 'join_reseller';
      options.bcc = ['fernando@makeupclubapp.com'];
      accountSchema.mailer.send(view,options);
    }
}

//accountSchema.post("save", sendEmailWhenRegister);

module.exports = function(mailer) {
  accountSchema.mailer = mailer;
  return Account = mongoose.model('Account',accountSchema);
}
