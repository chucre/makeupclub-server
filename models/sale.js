'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema,
    timestamp = require('mongoose-timestamp'),
    removedTimestamp = require('mongoose-paranoid_remove');

var saleSchema = new Schema({
  client: { type: Schema.Types.ObjectId, ref: 'Client'},
  date_sale: Date,
  date_use: Date,
  is_paid: Boolean,
  is_realized: Boolean,
  items: [{
    product: { type: Schema.Types.ObjectId, ref: 'Product'},
    quantity: Number,
    price: Number
  }],
  total_products_value: Number,
  total_discount: Number,
  total_sale_value: Number,
  method_of_payment: String,
  installments: [{
    value: Number,
    is_paid: Boolean,
    date: Date
  }],
  account: { type: Schema.Types.ObjectId, ref: 'Account' }
});
saleSchema.plugin(timestamp);
saleSchema.plugin(removedTimestamp);
var Sale = mongoose.model('Sale',
  saleSchema
);

module.exports = Sale;
