'use strict';
var SECS_TO_EXPIRE = 4*60*60, //4 hours
    hat = require('hat');

var randToken = function() {
  return hat.rack(1024)();
}

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema,
    mailer   = require('./mailer.js'),
    Account  = require('./account.js')(mailer);

var tokenSchema =  new mongoose.Schema({
  account: { type: Schema.Types.ObjectId, ref: 'Account' },
  app: String,
  token: { type: String, unique: true },
  validUntil: Date
});

tokenSchema.statics.valid = function(token) {
  return this.findOne({token: token, validUntil: {"$gte": new Date()}});
}
tokenSchema.statics.createNew = function(account, app) {
  var valid = new Date();
  if (!app) {
    app = "makeupclub"
  } 
  valid.setSeconds(valid.getSeconds()+SECS_TO_EXPIRE);
  Account.findOne({_id: account})
  .execQ()
  .then(function(current_account){
    if (current_account.invited_by.length>0) {
      for (var i =0; i < current_account.invited_by.length; i++) {
        Account.addPoint(current_account.invited_by[i], Account.points.INVITE_JOINED, 'Amigo entrou no jogo', {account: current_account._id});
      }
      current_account.invited_by = [];
      current_account.saveQ();
    }
  })
  return this.createQ({validUntil: valid, account: account, token: randToken(), app: app});
}

module.exports = mongoose.model('Token',tokenSchema);
