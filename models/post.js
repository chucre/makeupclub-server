'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema;

var postSchema = new Schema({
  publisher: { type: Schema.Types.ObjectId, ref: 'Account' },
  date: Date,
  title: String,
  description: String,
  image: String,
  video_id: String,
  channel: {
    id: String,
    name: String,
    image: String
  }
});

postSchema.statics.getRecents = function(lastPost){
  var query = {};
  if (lastPost) {
    query.date = {"$lt": new Date(lastPost)};
  }
  return this.find(query).sort({date: 'desc'}).limit(5);
}

var Post = mongoose.model('Post',postSchema);

module.exports = Post;
