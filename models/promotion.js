'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema;

var promotionSchema = new Schema({
  date: Date,
  account: { type: Schema.Types.ObjectId, ref: 'Account' },
  title: String,
  description: String,
  price: {
    old: Number,
    current: Number
  },
  total: Number,
  remaining: Number,
  coupons: [
    {
      code: String,
      account: { type: Schema.Types.ObjectId, ref: 'Account' },
      rating: Number,
      attended: Boolean,
      messages: [
        {
          from: { type: Schema.Types.ObjectId, ref: 'Account' },
          message: String,
          date: Date
        }
      ]
    }
  ],
  schedules: [{ type: Schema.Types.ObjectId, ref: 'Account' }]
});

var Promotion = mongoose.model('Promotion',
  promotionSchema
);


module.exports = Promotion;
