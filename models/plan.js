'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema = mongoose.Schema;

var planSchema = new Schema({
  name: String,
  detail: String,
  original_price: Number,
  price: Number,
  featured: Boolean,
  active: Boolean,
  promotion_code: String,
  periodicity: Number,
});

planSchema.statics.periodicity = [
  {
    pagseguro_value: 'MONTHLY',
    label: 'Mensal',
    valid_base_in_month: 1
  },
  {
    pagseguro_value:'TRIMONTHLY',
    label: 'Trimestral',
    valid_base_in_month: 3
  },
  {
    pagseguro_value:'SEMIANNUALLY',
    label: 'Semestral',
    valid_base_in_month: 6
  },
  {
    pagseguro_value:'YEARLY',
    label: 'Anual',
    valid_base_in_month: 12
  }
];

var Plan = mongoose.model('Plan',
  planSchema
);

module.exports = Plan;
