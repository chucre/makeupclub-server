'use strict';

var mongoose = require('mongoose-q')(require('mongoose')),
    Schema   = mongoose.Schema,
    Q        = require('q'),
    mailer   = require('./mailer.js'),
    Account  = require('./account.js')(mailer),
    Brand    = require('./brand.js'),
    Post     = require('./post.js'),
    Buy;

var buySchema =  new mongoose.Schema({
  account: { type: Schema.Types.ObjectId, ref: 'Account' },
  reseller: { type: Schema.Types.ObjectId, ref: 'Account' },
  brand: { type: Schema.Types.ObjectId, ref: 'Brand' },
  post: { type: Schema.Types.ObjectId, ref: 'Post' },
  created_at: Date
});

var chooseReseller = function(next) {
  this.created_at = new Date();
  var self = this;
  this.populate({path: 'account', model:'Account'},function(err, buy){
    var account = buy.account;
    Account
    .where({reseller: true, location: account.location, brands : mongoose.Types.ObjectId(self.brand)})
    .sort({lastDraw: 'asc'})
    .findOneQ()
    .then(function(reseller){
      self.reseller = reseller._id;
    });
    next();
    
  });
}

var sendEmailToCustomerAndReseller = function() {
  var buy = this;

  Q.all([
    Account.findOneQ({_id: buy.account}),
    Account.findOneQ({_id: buy.reseller}),
    Post.findOneQ({_id: buy.post}),
    Brand.findOneQ({_id: buy.brand})
  ])
  .then(function(results){
    var account = results[0],
        reseller = results[1],
        post = results[2],
        brand = results[3],
        view = '',
        options = {
          from: 'Mirla Braga<mirla@makeupclubapp.com>',
          to: account.email,
          subject: '',
          post: post,
          account: account,
          reseller: reseller,
          brand: brand
        };

    if(reseller) {
        view  = "buy_customer_with_reseller";
        options.subject = "Uma revendedora entrará em contato em breve";
    }else {
      view  = "buy_customer_without_reseller";
      options.subject = "Estou buscando uma revendedora para você";
      options.bcc = 'fernandochucre@gmail.com';
    }

    buySchema.mailer.send(view,options);

    options.to = reseller.email;

    view = 'buy_reseller_known_customer';
    options.subject = 'Você tem uma nova visita para agendar';
    buySchema.mailer.send(view,options);
  });
}

buySchema.pre("save", chooseReseller);
buySchema.post("save", sendEmailToCustomerAndReseller);

module.exports = function(mailer) {
  buySchema.mailer = mailer;
  return Buy = mongoose.model('Buy',buySchema);
}
