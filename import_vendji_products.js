var Product = require('./models/product.js'),
		fs = require("fs"),
    path = require("path"),
    mongoUrl = process.env.MONGOHQ_URL || process.env.MONGO_URL || "mongodb://localhost:27017/tests",
    mongoose = require('mongoose');

mongoose.connect(mongoUrl);;

var p = "../vendji_crawler/products/";

fs.readdir(p, function (err, files) {
    if (err) {
        throw err;
    }

    files.map(function (file) {
        return path.join(p, file);
    }).filter(function (file) {
        return fs.statSync(file).isFile();
    }).forEach(function (file) {
        var obj = JSON.parse(fs.readFileSync(file, 'utf8'));
        var model = new Product(obj);
        model.saveQ()
        .then(function(product){
        	console.log(product.name);
        })
        .catch(function(err){
        	console.log(err.toString()+". ("+obj.id+")");
        });
    });
});
