var mailer = require('./models/mailer.js'),
  Account = require('./models/account.js')(mailer),
  mongoUrl = process.env.MONGOHQ_URL || process.env.MONGO_URL || "mongodb://localhost:27017/tests",
  mongoose = require('mongoose');

mongoose.connect(mongoUrl);

Account.where({'password':{$exists: true}})
.execQ()
.then(function(accounts){
  accounts.forEach(function(account){
    var view,
        options = {
          from: 'Luana Caetano [MakeupClub App]<luana@makeupclubapp.com>',
          to: account.email,
          subject: "Não foi culpa sua!",
          account: account
        };
    if (account.devices.length>0) {
      view = 'mailer/extras/apologize02';
    } else {
      view = 'mailer/extras/apologize01';
      account.validUntil = new Date(new Date().getTime()+30*24*60*60*1000);
      account.save();
    }
    console.log(account.email);
    mailer.send(view, options);
  });
})
