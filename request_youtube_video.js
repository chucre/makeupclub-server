'use strict';
var youtube  = require('youtube-api'),
    mongoUrl = process.env.MONGOHQ_URL || process.env.MONGO_URL || "mongodb://localhost:27017/tests",
    mongoose = require('mongoose'),
    Post     = require('./models/post.js'),
    Account  = require('./models/account.js'),
    account_id  = process.env.ACCOUNT_ID || '53dd6741d975709e282616fd',
    videos_list;

videos_list = process.argv.slice(2);
mongoose.connect(mongoUrl);

youtube.authenticate({
    type: 'key',
    key: 'AIzaSyCUh3-ObBIQGMJlulEq2HCfORFdtMp7FCY'
});

for(var i in videos_list) {
  youtube.videos.list({
      part: 'id,snippet',
      id: videos_list[i]
    },
    function(err, data){
      if (data.items.length==0) {
        console.log('Video not found: '+videos_list[i]);
        return;
      }
      var video = data.items[0];
      var post = {
        publisher: account_id,
        date: new Date(),
        title: video.snippet.title,
        description: video.snippet.description,
        video_id: video.id,
        image: video.snippet.thumbnails.high.url,
        channel: {
          id: video.snippet.channelId
        }
      }
      youtube.channels.list({
        part: 'id,snippet',
        id: video.snippet.channelId
      }, function(err, data){
        if (err) {
          console.log('error on get channel: '+err);
          console.log('channel not found: '+video.channelId);
          return;
        }
        if (data.items.length==0) {
          console.log('channel not found: '+video.channelId);
          return;
        }
        var channel = data.items[0];
        post.channel.name = channel.snippet.title;
        post.channel.image = channel.snippet.thumbnails.medium.url;
        Post.createQ(post)
        .then(function(){
          console.log("created post for video:"+post.video_id);
        })
        .catch(function(err){
          console.log('some error on create post for video:'+post.video_id+". "+err);
        });
      })
    }
  )
}

