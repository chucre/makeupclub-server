set :use_sudo, false

set :application, 'makeupclubapp'
set :repo_url, 'git@bitbucket.org:chucre/makeupclub-server.git'
set :branch, 'master'
set :scm, :git
set :deploy_via, :remote_cache
set :default_stage, 'staging'
set :user, 'deploy'
set :keep_releases, 5
set :rvm_type, :user
set :rails_env, 'production'
set :rvm_ruby_version, '1.9.3-p484@taxisimples'
set :linked_dirs, ['node_modules', 'public/images']

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute "cd #{release_path}; npm install;"
      execute "cd #{release_path}; NODE_MONGOOSE_MIGRATIONS_CONFIG=./config/migration.js NODE_ENV=#{fetch(:rails_env)} ./node_modules/.bin/mongoose-migrate "
      execute "cd #{release_path}; sudo stop makeupclub-server-#{fetch(:stage)}; sudo start makeupclub-server-#{fetch(:stage)}"
      execute "rm -rf #{release_path}/public/painel; ln -s #{fetch(:makeupclub_painel_path)}/current/www #{release_path}/public/painel"
    end
  end

  after :deploy, "deploy:restart"
  after :finishing, 'deploy:cleanup'

end
