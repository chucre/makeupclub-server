set :stage, :production

role :app, %w{deploy@72.14.176.48}
role :web, %w{deploy@72.14.176.48}
role :db,  %w{deploy@72.14.176.48}
set :deploy_to, "/var/makeupclub/server"
set :makeupclub_painel_path, "/var/makeupclub/painel"
