'use strict';

var express    = require('express'),
    app        = express(),
    session    = require('express-session'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    flash    = require('connect-flash'),
    enrouten   = require('express-enrouten'),
    port       = process.env.PORT || 3000,
    tokenAuthentication = require('./midlewares/tokenAuthentication.js'),
    expressValidator = require('express-validator'),
    mongoUrl = process.env.MONGOHQ_URL || process.env.MONGO_URL || "mongodb://localhost:27017/tests",
    mongoose = require('mongoose'),
    passport = require('passport'),
    localStrategy = require('./libs/passport.js')(passport),
    Agenda = require('agenda');


var agenda = new Agenda({db: { address: mongoUrl, collection: 'agendaJobs' }}),
    agendaMailer = require('./schedules/userMailer.js')(agenda),
    agendaPusher = require('./schedules/push_notifications.js')(agenda);

agenda.processEvery('one minute');

agendaPusher.pravenderCrawler(function(){},function(){}); 

agenda.start();

mongoose.connect(mongoUrl);

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.all('/*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
  //res.header("Access-Control-Allow-Origin", "https://sandbox.pagseguro.uol.com.br");
  next();
});

var oneDay = 86400000;

app.use(express.static('public',  { maxAge: oneDay }));
app.use(session({ secret: 'keyboard cat' }));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(cookieParser());
app.use(bodyParser());
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));
app.use(flash()); // use connect-flash for flash messages stored in session

app.use(expressValidator());
app.use(tokenAuthentication);
app.use(enrouten({ directory: 'controllers', index: 'controllers/index.js' }));
app.listen(port, function(){
  console.log('Server started on port ' + port);
});
