var mongoUrl = process.env.MONGOHQ_URL || process.env.MONGO_URL || "mongodb://localhost:27017/tests",
  mongoose = require('mongoose'),
  mailer = require('./models/mailer'),
  Buy     = require('./models/buy.js')(mailer);

mongoose.connect(mongoUrl);

Buy.find()
.sort('-created_at')
.limit(7)
.populate('brand', 'name')
.populate('account', 'name phone location')
.execQ()
.then(function(buys){
  console.log(buys);
  process.exit();
})
