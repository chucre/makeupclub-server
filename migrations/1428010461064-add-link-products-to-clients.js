var mailer = require('../models/mailer.js'),
    Account = require('../models/account.js')(mailer),
    Brand = require('../models/brand.js'),
    Q = require('q');

exports.up = function(next){
  Brand.findOne({name: 'Mary Kay'}).execQ()
  .then(function(brand){
    Account.updateQ({password: {$exists: true}}, {$set: { brands: [brand]} },{ multi: true })
    .then(function(){
      next();
    })
  })
  .catch(next);
};

exports.down = function(next){
  next();
};
