var Brand = require('../models/brand.js'),
    Q = require('q');

exports.up = function(next){
  Brand.removeQ({});
  Q.all([
    new Brand({name: "Mary Kay"}).saveQ(),
    new Brand({name: "Natura"}).saveQ(),
    new Brand({name: "Boticário"}).saveQ(),
    new Brand({name: "Jequiti Colheita"}).saveQ(),
    new Brand({name: "Eudora"}).saveQ(),
    new Brand({name: "Jequiti Villa"}).saveQ(),
    new Brand({name: "Avon Cosméticos"}).saveQ(),
  ])
  .then(function(){
    next();
  })
};

exports.down = function(next){
  Brand.removeQ({});
  next();
};