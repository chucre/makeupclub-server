var Brand = require('../models/brand.js'),
    Product = require('../models/product.js'),
    Q = require('q');

exports.up = function(next){
  Q.all([
    Brand.findOne({name: 'Mary Kay'}).execQ(),
    Brand.findOne({name: 'Natura'}).execQ(),
    Brand.findOne({name: 'Boticário'}).execQ(),
    Brand.findOne({name: 'Jequiti Colheita'}).execQ(),
    Brand.findOne({name: 'Eudora'}).execQ(),
    Brand.findOne({name: 'Jequiti Villa'}).execQ(),
    Brand.findOne({name: 'Avon Cosméticos'}).execQ(),
  ])
  .then(function(brands){
    Q.all([
      Product.updateQ({catalog_name: brands[0].name},{brand: brands[0]},{ multi: true }),
      Product.updateQ({catalog_name: brands[1].name},{brand: brands[1]},{ multi: true }),
      Product.updateQ({catalog_name: brands[2].name},{brand: brands[2]},{ multi: true }),
      Product.updateQ({catalog_name: brands[3].name},{brand: brands[3]},{ multi: true }),
      Product.updateQ({catalog_name: brands[4].name},{brand: brands[4]},{ multi: true }),
      Product.updateQ({catalog_name: brands[5].name},{brand: brands[5]},{ multi: true }),
      Product.updateQ({catalog_name: brands[6].name},{brand: brands[6]},{ multi: true })
    ])
    .then(function(){
      next();
    });
  })
  .catch(function(err){
    console.log(err.toString());
    next(err);
  });
};

exports.down = function(next){
  next();
};
