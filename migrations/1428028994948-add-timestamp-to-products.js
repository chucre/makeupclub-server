var Product = require('../models/product.js');

exports.up = function(next){
  Product.updateQ({},{createdAt: new Date(), updatedAt: new Date()}, {multi: true})
  .then(function(){
    next();
  })
  .catch(next);
};

exports.down = function(next){
  next();
};
