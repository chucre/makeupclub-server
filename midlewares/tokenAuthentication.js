'use strict';
var Account = require('../models/account.js'),
    Token = require('../models/token.js')

var tokenAuthentication = function(req, res, next) {
  var token = req.query.token || req.body.token;
  if (token) {
    return Token.valid(token).execQ()
    .then(function(token){
      req.account = token.account;
      next();
    })
    .catch(function(err){
      console.log(err.toString());
      res.json(401,{error: 'invalid token'});
    })
  } else {
    next();
  }
}

module.exports = tokenAuthentication;
