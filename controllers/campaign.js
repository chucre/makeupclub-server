var campaign = function(req, res) {
  res.render('campaign/' + req.params.campaign);
};

module.exports = function (router) {
  router.get('/:campaign', campaign);
};
