var Sale = require('../models/sale.js');

var updateSale = function(req, res) {
  if (!req.account) {
    res.json(400, {
      error: 'invalid credentials'
    });
  } else {
    var id = req.params.id;
    delete req.body._id;

    Sale.findOne({_id: id})
    .execQ()
    .then(function(sale) {
      sale.set(req.body);
      return sale.saveQ()
      .then(function(){
        res.json(200, sale);
      });
    })
    .catch(function(error) {
      res.json(400, {
        error: error.toString()
      });
    });
  }
};

var removeSale = function(req, res) {
  if (!req.account) {
    res.json(400, {
      error: 'invalid credentials'
    });
  } else {
    var id = req.params.id;
    Sale.findOne({_id: id})
    .execQ()
    .then(function(sale) {
      if (sale) {
        sale.paranoid_remove();
        return sale.saveQ()
        .then(function(){
          res.json(200, {});
        });
      } else {
        res.json(404, {});
      }
    })
    .catch(function(error) {
      res.json(400, {
        error: error.toString()
      });
    });
  }
};

var createSale = function(req, res) {
  if (!req.account) {
    res.json(400, {
      error: 'invalid credentials'
    });
  } else {
    req.checkBody('client').notEmpty();
    req.checkBody('date_sale').notEmpty();
    req.checkBody('items').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
      res.json(400, {
        errors: errors
      });
    } else {
      var sale = new Sale(req.body);
      sale.account = req.account;
      sale.saveQ().then(function(sale) {
          res.json(200, sale);
        })
        .catch(function(error) {
          res.json(400, {
            error: error.toString()
          });
        });
    }
  }
};


module.exports = function(router) {
  router.post('/:id', updateSale);
  router.post('/', createSale);
  router.delete('/:id', removeSale);
};
