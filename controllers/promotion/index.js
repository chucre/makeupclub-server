var Promotion       = require('../../models/promotion.js'),
    moment          = require('moment'),
    mailer          = require('../../models/mailer.js'), 
    Account         = require('../../models/account.js')(mailer);

var index = function(req, res) {

  var date = req.query.date || new Date();
  var start = moment(date);
  start.seconds(0);
  start.minutes(0);
  start.hours(0);

  var end = moment(date);
  end.seconds(59);
  end.minutes(59);
  end.hours(23);

  var nexts = req.query.nexts || false;

  var find;
  if (nexts) {
    find = Promotion.find({date: {"$gt": end}}).limit(10)
  } else {
    find = Promotion.find({date: {"$gte": start, "$lt": end}})
  }

  find
  .sort('date')
  .populate('account','name address thumb phone')
  .execQ()
  .then(function(promotions){
    res.json(200,promotions);
  }).catch(function(error){
    res.json(500,error);
  });
}

var get_promotion = function(req, res) {
  Promotion.findOne({_id: req.params.id})
  .populate('account','name address thumb phone')
  .execQ()
  .then(function(promotion){
    res.json(200,promotion);
  })
  .catch(function(){
    res.json(404);
  })
}

var validate = function(req) {
  req.checkBody('date').notEmpty();
  req.checkBody('title').notEmpty();
  req.checkBody('description').notEmpty();
  req.checkBody('price.old').notEmpty();
  req.checkBody('price.current').notEmpty();
  req.checkBody('total').notEmpty();
}
var create = function(req, res) {
  if (!req.account) {
    return res.json(400,{error: 'invalid credentials'})
  }
  validate(req);

  var errors = req.validationErrors();
  if (errors) {
    res.json(400,{errors: errors});
  } else {
    var promo = req.body;
    promo.date = promo.date+'T12:00:00Z';
    promo.account = req.account;
    Promotion.createQ(promo)
    .then(function(promo){
      res.json(200, promo);
    })
    .catch(function(error){
      res.json(400, {error: error.toString()});
    })
  }
}

var update = function(req, res) {
  if (!req.account) {
    return res.json(400,{error: 'invalid credentials'})
  }

  validate(req);
  var promo = req.body;
  promo.account = req.account;
  Promotion.updateQ({_id: req.params.id, account: req.account}, { $set: promo })
  .then(function(){
    res.json(200, promo);
  })
  .catch(function(error){
    res.json(400, error);
  })
}

var register = function(req, res) {
  if (!req.account) {
    return res.json(400,{error: 'invalid credentials'})
  }

  var promo = req.params.id;
  Promotion.findOne({_id: promo}).execQ()
  .then(function(promotion){
    promotion.coupons = promotion.coupons || [];
    for(var i in promotion.coupons) {
      if (promotion.coupons[i].account == req.account.toString() ) {
        return res.json(200, {code: promotion.coupons[i].code});
      }
    }

    var code = promo.substring(19)+'-'+(promotion.coupons.length+1);
    promotion.coupons.push({
      code: code,
      account: req.account
    })
    promotion.remaining = promotion.total - promotion.coupons.length;
    promotion.saveQ()
    .then(function(promotion){
      Account.addPoint(req.account, Account.points.PROMO_CODE, 'Gerar código da promoção', {place: promotion.account});
      res.json(200, {code: code});
    })
  })
  .catch(function(error){
    res.json(400,{error: error.toString()});
  })
}

var schedule = function(req, res) {
  if (!req.account) {
    return res.json(400,{error: 'invalid credentials'})
  }

  var promo = req.params.id;
  Promotion.findOne({_id: promo}).execQ()
  .then(function(promotion){
    for(var i in promotion.schedules) {
      if (promotion.schedules[i] == req.account.toString()) {
        return res.json(200, {scheduled: true});
      }
    }
    promotion.schedules.push(req.account);
    promotion.saveQ()
    .then(function(promotion){
      Account.addPoint(req.account, Account.points.SCHEDULE, 'Agendar promoção', {place: promotion.account});
      res.json(200, {scheduled: true});
    })
    .catch(function(error){
      res.json(400,{error: error.toString()});
    })
  })
  .catch(function(error){
    res.json(400,error);
  })
}

var replyRating = function(req, res) {
  if (!req.account) {
    return res.json(400,{error: 'invalid credentials'})
  }

  var promo = req.params.id;
  Promotion.findOne({_id: promo}).execQ()
  .then(function(promotion) {
    var coupon = promotion.coupons.id(req.params.id_coupon);
    
    if(!promotion.account == req.account.toString() || !coupon) {
      return res.json(404);
    }
    if (req.body.message) {
      coupon.messages.push({
        from: req.account,
        message: req.body.message,
      });
      coupon.markModified('messages');
    }

    promotion.saveQ()
    .then(function(coupon){
      res.json(200, coupon);
    })
    .catch(function(error){
      res.json(400,{error: error.toString()});
    })
  })
  .catch(function(error){
    res.json(400,{error: error.toString()});
  })
}

var rating = function(req, res) {
  if (!req.account) {
    return res.json(400,{error: 'invalid credentials'})
  }

  var promo = req.params.id;
  Promotion.findOne({_id: promo}).execQ()
  .then(function(promotion) {
    var coupon;
    for(var i in promotion.coupons) {
      if (promotion.coupons[i].account == req.account.toString()) {
        coupon = promotion.coupons[i];
        break;
      }
    }
    if(!coupon) {
      return res.json(404);
    }

    coupon.rating = req.body.rating;
    coupon.markModified('rating');
    if (req.body.message) {
      coupon.messages.push({
        from: req.account,
        message: req.body.message,
      });
      coupon.markModified('messages');
    }

    promotion.saveQ()
    .then(function(coupon){
      res.json(200, coupon);
    })
    .catch(function(error){
      res.json(400,{error: error.toString()});
    })
  })
  .catch(function(error){
    res.json(400,{error: error.toString()});
  })
}

var attending = function(req, res) {
  if (!req.account) {
    return res.json(400,{error: 'invalid credentials'})
  }

  var promo = req.params.id;
  Promotion.findOne({_id: promo}).execQ()
  .then(function(promotion) {
    var coupon;
    for(var i in promotion.coupons) {
      if (promotion.coupons[i].code == req.body.code) {
        coupon = promotion.coupons[i];
        break;
      }
    }
    if(!coupon) {
      return res.json(404);
    }

    if (coupon.attended) {
      return res.json(200, coupon);
    }

    coupon.attended = true;
    coupon.markModified('attended');
    
    promotion.saveQ()
    .then(function(){
      Account.addPoint(coupon.account, (Account.points.PROMO_USED*promotion.price.current), 'Usar código da promoção', {Promotion: promotion._id});
      res.json(200, coupon);
    })
    .catch(function(error){
      res.json(400,{error: error.toString()});
    })
  })
  .catch(function(error){
    res.json(400,{error: error.toString()});
  })
}

module.exports = function (router) {
  router.get('/', index);
  router.get('/:id', get_promotion);
  router.post('/', create);
  router.post('/:id', update);
  router.post('/:id/coupon', register);
  router.post('/:id/schedule', schedule);
  router.post('/:id/rating', rating);
  router.post('/:id/attending', attending);
  router.post('/:id/coupon/:id_coupon/reply', replyRating);

  return {
    index: index
  }
};
