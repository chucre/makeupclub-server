var mailer    = require('../../models/mailer.js'),
    Buy       = require('../../models/buy.js')(mailer),
    Brand     = require('../../models/brand.js'),
    Post      = require('../../models/post.js');
    Account   = require('../../models/account.js');

var buy = function(req, res) {
  var brand;

  if (!req.account) {
    res.json(400,{error: 'invalid credentials'})
  } else {
    return Brand.find({_id: req.body.brand_id})
    .execQ()
    .catch(function(){
      throw 'invalid brand';
    })
    .then(function(_brand) {
      brand = _brand;
      return Post.find({_id: req.body.post_id})
      .execQ()
      .catch(function(){
        throw 'invalid post';
      })
    })
    .then(function(post){
      var buy = {
        brand: req.body.brand_id,
        account: req.account,
        post: req.body.post_id
      }
      return Buy.createQ(buy);
    })
    .then(function(buy){
      res.json(200,buy)
    })
    .catch(function(error){
      res.json(400,{error: error})
    })
  }
}

module.exports = function (router) {
  router.post('/', buy);

  return {
    buy: buy
  }
};
