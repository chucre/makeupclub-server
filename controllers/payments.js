var passport = require('passport'),
    localStrategy = require('../libs/passport.js')(passport),
    pagseguro = require('../libs/pagseguro.js'),
    Plan  = require('../models/plan.js'), 
    mailer  = require('../models/mailer.js'), 
    Account = require('../models/account.js')(mailer);

var included = function(array, item) {
  return array.indexOf(item)>=0;
};

var calculateValidUntil = function(plan, date) {
  date.setMonth(date.getMonth()+Plan.periodicity[plan.periodicity].valid_base_in_month);
  return date;
};

var pagseguroCallback = function(req, res) {
  var notificationType = req.body.notificationType || req.query.notificationType;
  var notificationCode = req.body.notificationCode || req.query.notificationCode;
  console.log(notificationCode);
  console.log(notificationType);
  
  pagseguro.getTransaction(notificationType, notificationCode)
  .then(function(transaction){
    console.log(transaction);
    return Account.findOne({_id: transaction.reference[0]})
    .populate('plan', 'id periodicity')
    .execQ()
    .then(function(account){
      account.transactions.push(transaction);
      if (included(['CANCELLED', 'CANCELLED_BY_RECEIVER', 'CANCELLED_BY_SENDER'], transaction.status[0])) {
        account.plan = undefined;
        account.validUntil = undefined;
      } else if(transaction.status[0]=='ACTIVE') {
        account.validUntil = calculateValidUntil(account.plan, new Date(transaction.date[0]));
      }
      return account.saveQ();
    });
  })
  .then(function(){
    res.send('ok');
  })
  .catch(function(err){
    res.send(err.toString());
  });
};
module.exports = function (router) {
  router.post('/callback', pagseguroCallback);
  router.get('/callback', pagseguroCallback);
};
