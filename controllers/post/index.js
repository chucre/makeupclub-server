var Post = require('../../models/post.js');

var index = function(req, res) {
  if (!req.account) {
    res.json(400,{error: 'invalid credentials'})
  } else {
    var lastPost = req.query.lastPost || undefined;

    return Post.getRecents(lastPost).execQ()
    .then(function(posts){
      res.json(200,posts)
    })
  }
}

module.exports = function (router) {
  router.get('/', index);

  return {
    index: index
  }
};
