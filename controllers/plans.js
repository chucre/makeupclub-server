var Plan = require('../models/plan.js'),
    passport = require('passport'),
    localStrategy = require('../libs/passport.js')(passport),
    pagseguro = require('../libs/pagseguro.js');

var index = function(req, res) {
  var promotion_code;
  
  if(req.query.promotion_code){
    promotion_code = new RegExp("^"+req.query.promotion_code+"$", "i");
  } else {
    promotion_code = '';
  }
  Plan.find({active:true, promotion_code: promotion_code})
  .execQ()
  .then(function(plans){
    res.format({
      json: function() {
        res.json(200, plans);
      },
      html: function() {
        res.render('plans/index.jade',{plans: plans, Plan: Plan});
      },
    });
  })
  .catch(function(err){
    res.format({
      json: function() {
        res.json(400, {error: err.toString()});
      },
      html: function() {
        res.redirect('/');
      },
    });
  });
};

var applyPlan = function(req, res) {
  Plan.findOne({_id: req.body.plan_id})
  .execQ()
  .then(function(plan){
    if (plan) {
      pagseguro.generatePlan(req.account, plan)
      .then(function(pagseguro_response){
        req.account.plan = plan;
        return req.account.saveQ()
        .then(function(){
          var url = pagseguro.getPaths().PROCESS_APPROVALS+'?code='+pagseguro_response.code;
          res.format({
            json: function() {
              res.json(200, {url: url});
            },
            html: function() {
              res.redirect(url);
            },
          });  
        });
      })
      .catch(function(err){
        res.format({
          json: function() {
            res.json(400, {error: err.toString()});
          },
          html: function() {
            res.send(err);
          },
        });
      });
    } else {
      res.format({
        json: function() {
          res.json(400, {error: 'Select a plan'});
        },
        html: function() {
          res.redirect('/account');
        },
      });
    }
  })
  .catch(function(err){
    res.format({
      json: function() {
        res.json(400, {error: err.toString()});
      },
      html: function() {
        res.redirect('/account');
      },
    });
  });
};

module.exports = function (router) {
  router.get('/', passport.ensureLogged, index);
  router.post('/apply', passport.ensureLogged, applyPlan);
};
