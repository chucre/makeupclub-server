var Task = require('../models/task.js');

var createTask = function(req, res) {
  if (!req.account) {
    res.json(400, {
      error: 'invalid credentials'
    });
  } else {
    var task = new Task(req.body);
    task.account = req.account;
    task.saveQ().then(function(task) {
        res.json(200, task);
      })
      .catch(function(error) {
        res.json(400, {
          error: error.toString()
        });
      });
  }
};

var updateTask = function(req, res) {
  if (!req.account) {
    res.json(400, {
      error: 'invalid credentials'
    });
  } else {
    var id = req.params.id;
    delete req.body._id;
    Task.findOne({_id: id}).execQ()
    .then(function(task) {
      task.set(req.body);
      return task.saveQ()
      .then(function(){
        res.json(200, task);
      });
    })
    .catch(function(error) {
      res.json(400, {
        error: error.toString()
      });
    });
  }
};

var removeTask = function(req, res) {
  if (!req.account) {
    res.json(400, {
      error: 'invalid credentials'
    });
  } else {
    var id = req.params.id;
    Task.findOne({_id: id}).execQ()
    .then(function(task) {
      if (task) {
        task.paranoid_remove();
        return task.saveQ()
        .then(function(){
          res.json(200, {});
        });
      } else {
        res.json(404, {});
      }
    })
    .catch(function(error) {
      res.json(400, {
        error: error.toString()
      });
    });
  }
};


module.exports = function(router) {
  router.post('/:id', updateTask);
  router.post('/', createTask);
  router.delete('/:id', removeTask);
};
