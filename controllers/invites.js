var mailer  = require('../models/mailer.js'), 
    Account = require('../models/account.js')(mailer),
    Q       = require('q'),
    FB      = require('fb');

var invite_on_fb = function(account, fbId, token) {
  var link_message = {
    link: 'http://www.lookmeup.com.br/',
    message: 'Vem ficar linda comigo tb no LookMe UP!',
    access_token: token,
    place: '842645532420467',
    tags: fbId.join(',')
  }

  FB.api('/me/feed', 'post', link_message);
}

var invite = function(req, res) {
  if (!req.account) {
    res.json(400,{error: 'invalid credentials'})
  } else {
    var fbIds = req.body.fbIds;
    var fbAccountsExits = []
    Account.find({'facebook.id': { $in: fbIds } })
    .execQ()
    .then(function(accounts){
      for (var i in accounts) {
        var account = accounts[i];
        fbAccountsExits.push(account.facebook.id);
        if (account.invited_by.indexOf(req.account.toString()) == -1) {
          account.invited_by.push(req.account);
          account.saveQ();
          Account.addPoint(req.account, Account.points.INVITE, 'Convidar amigo', {fbId: account.facebook.id});
        }
      }
      for (var i in fbIds) {
        if (fbAccountsExits.indexOf(fbIds[i]) == -1) {
          var account = {
            invited_by: [req.account],
            facebook: {
              id: fbIds[i]
            }
          }
          Account.createQ(account);
          Account.addPoint(req.account, Account.points.INVITE, 'Convidar amigo', {fbId: fbIds[i]});
        }
      }
      invite_on_fb(req.account, fbIds, req.body.fbToken);
      return true;
    })
    .then(function(){
      res.json(200, {invited: true})
    })
    .catch(function(err){
      res.json(500,{erro: err.toString()})
    })
  }
}

module.exports = function (router) {
  router.post('/', invite);

  return {
    invite: invite
  }
};
