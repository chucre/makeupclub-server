var mailer = require('../../models/mailer.js'),
  Account = require('../../models/account.js')(mailer),
  Token = require('../../models/token.js'),
  SocialConnector = require('../../models/socialconnector.js'),
  Q = require('q'),
  passport = require('passport'),
  localStrategy = require('../../libs/passport.js')(passport);;

var findAccountByFacebook = function(req, res) {
  var fbData;
  return SocialConnector.connect(req.body.network)
    .then(function(data) {
      fbData = data;
      return Account.findOne({
        'facebook.id': data.id
      }).execQ();
    })
    .then(function(account) {
      if (!account) {
        var data = {
          name: fbData.name,
          email: fbData.email,
          location: fbData.location,
          apps: [app]
        }
        data[req.body.network.name] = fbData;
        return Account.createQ(data);
      } else {
        return account;
      }
    })
}

var findAccountByMakeup = function(req, res) {
  var email = req.body.email;
  return Account.findOne({
      email: email
    }).execQ()
    .then(function(account) {
      if (account.validPassword(req.body.password)) {
        return account;
      } else {
        return null;
      }
    });
}

var create = function(req, res) {
  var fbData;
  var app = req.body.app || 'makeupclub';
  var network = req.body.network || 'makeupclub';

  var promise;
  if (network == 'facebook') {
    promise = findAccountByFacebook(req, res);
  } else {
    promise = findAccountByMakeup(req, res);
  }

  promise
    .then(function(account) {
      if (!account) {
        throw new Error('account not found')
      }
      return Token.createNew(account, app)
      .then(function(token) {
        var result = {
          token: token.token,
          validUntil: token.validUntil,
          paid: true
        }
        if (account.validUntil > token.validUntil) {
          result.validUntil = account.validUntil;
          token.validUntil = account.validUntil;
          token.save();
        } else {
          result.paid = false;
        }
        res.json(200, result);
      })
    })
    .catch(function(err) {
      res.json(401, {
        error: err.toString()
      })
    })
}

var loginForm = function(req, res) {
  res.render('session/new.jade');
}

var logout = function(req, res) {
  req.logout();
  res.redirect('/');
}

module.exports = function(router) {
  router.post('/', create);
  router.get('/new', loginForm);
  router.get('/destroy', logout);
  router.post('/new', passport.authenticate('local', { 
    successRedirect: '/plans',
    failureRedirect: '/session/new',
    failureFlash: false
  }))
  return {
    create: create
  }
};
