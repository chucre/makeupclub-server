var mailer  = require('../models/mailer.js'), 
    Account = require('../models/account.js')(mailer),
    SocialConnector = require('../models/socialconnector.js');

var friendsList = function(req, res) {
  if (!req.account) {
    res.json(400,{error: 'invalid credentials'})
  } else {
    var me;
    Account.findOne({_id: req.account})
    .execQ()
    .then(function(account){
      me = account;
      return SocialConnector.friends(req.query.fbToken)
    })
    .then(function(friends){
      var fbIds = [];
      var friendMap = {};
      for (var i in friends) {
        friends[i].total_points = 0;
        fbIds.push(friends[i].id);
        friendMap[friends[i].id] = friends[i];
      }
      return Account.find({'facebook.id': {$in: fbIds}})
      .execQ()
      .then(function(accounts){
        for(var i in accounts) {
          var account = accounts[i];
          if (account.invited_by.length>0) {
            friendMap[account.facebook.id].total_points = 0;
          } else {
            friendMap[account.facebook.id].total_points = account.points.reduce(function(total, value){
              return total+value.points;
            },0);
          }
        }
        friends.push({
          me: true,
          name: me.name,
          id: me.facebook.id,
          total_points: me.points.reduce(function(total, value){
                          return total+value.points;
                        },0)
        });
        friends.sort(function(a,b){
          return b.total_points-a.total_points;
        })
        return friends;
      })
    })
    .then(function(friends){
      res.json(200, friends);
    })
    .catch(function(err){
      res.json(500,{error: err.toString()});
    })
  }
}

module.exports = function (router) {
  router.get('/', friendsList);

  return {
    friendsList: friendsList
  }
};
