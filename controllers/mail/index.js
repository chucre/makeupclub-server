var nodemailer = require('nodemailer');

var send = function(req, res) {

  req.checkBody('nome').notEmpty();
  req.checkBody('email').notEmpty();
  req.checkBody('mensagem').notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    res.json(400, {
      errors: errors
    });
  } else {

    var transporter = nodemailer.createTransport({
      service: 'SendGrid',
      auth: {
        user: 'makeupclub',
        pass: 'make@club#mail@2014'
      }
    });
    transporter.sendMail({
      from: req.body.email,
      to: 'fernandochucre@gmail.com',
      subject: 'Nova mensagem | MakeUpClub',
      text: req.body.mensagem
    }, function(error, info) {
      if (error) {
        console.log(error);
        res.json(400);
      } else {
        res.json(200);
      }
    });
  }
};



module.exports = function(router) {
  router.post('/send', send);

  return {
    send: send,
  };
};