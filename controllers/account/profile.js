var mailer = require('../../models/mailer.js'),
    passport = require('passport'),
    Account = require('../../models/account.js')(mailer),
    Brand = require('../../models/brand.js'),
    Invite = require('../../models/invite.js'),
    Q = require('q');

var index = function(req, res) {
  Q.all([
    req.account.populate('supervisor').populateQ('supervisedAccounts'),
    Brand.find().execQ(),
    req.account.invites().execQ(),
    Invite.findOne({email: req.account.email, is_managed: true}).populate('account').execQ()
  ])
  .then(function(results){
    var account = results[0];
    var brands = results[1];
    var invites = results[2];
    var pendent_invite = results[3];
    var promises = [];
    account.supervisedAccounts.forEach(function(account){
      promises.push(account.salesReport());
    });

    Q.all(promises)
    .then(function(results){
      account = account.toJSON();
      for(var i in results) {
        account.supervisedAccounts[i].total_sales = results[i].total;
        account.supervisedAccounts[i].week_sales = results[i].week;
        account.supervisedAccounts[i].month_sales = results[i].month;
      }
      account.invites = [];
      account.pendent_invite = (pendent_invite)?pendent_invite.toJSON():null;
      for(var i in invites) {
        account.invites.push(invites[i].toJSON());
      }
      res.format({
        json: function() {
          res.json(200, account);
        },
        html: function() {
          res.render('account/profile', {
            'profile': account, brands: brands
          });
        },
      });
    })
    .catch(function(err){
      res.format({
        json: function() {
          res.json(400, {error: err.getString()});
        },
        html: function() {
          res.render('account/profile', {
            'profile': account, brands: brands
          });
        },
      });
    })

  })
};

var edit = function(req, res) {
  req.account.set({
    brands: req.body.brands,
    name: req.body.name,
    phone: req.body.phone,
    email: req.body.email
  });
  Q.all([
    req.account.saveQ(),
    Brand.find().execQ()
  ])
  .then(function(results){
    res.format({
      json: function() {
        res.json(200, req.account);
      },
      html: function() {
        res.render('account/profile',{success: 'Perfil atualizado!', brands: results[1]});    
      },
    });
  })
  .catch(function(err){
    res.format({
      json: function() {
        res.json(400, {errors: [err]});
      },
      html: function() {
        res.render('account/profile',{errors: [err]});
      }
    })
  });
};

module.exports = function(router) {
  router.get('/', passport.ensureLogged, index);
  router.post('/', passport.ensureLogged, edit);
};
