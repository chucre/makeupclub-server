var mailer = require('../../models/mailer.js'),
    Account = require('../../models/account.js')(mailer),
    Client = require('../../models/client.js'),
    passport = require('passport'),
    localStrategy = require('../../libs/passport.js')(passport),
    Q = require('q');

var index = function(req, res) {
  var end = new Date();
  var start = new Date(end);
  start.setMonth(start.getMonth()-1);
  Q.all([
    req.account.tasks().populate('client').sort('start_at').where({deletedAt: {$exists: false}, is_realized: false}).execQ(),
    req.account.tasksByDay(start, end)  
  ])
  .then(function(results){
    res.render('account/tasks',{
      tasks: results[0], 
      tasks_report: results[1],
      moment: require('moment')
    });
  })
}

module.exports = function(router) {  
  router.get('/', passport.ensureLogged, index);
};
