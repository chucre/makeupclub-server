var mailer = require('../../models/mailer.js'),
  Account = require('../../models/account.js')(mailer),
  Sale = require('../../models/sale.js'),
  Client = require('../../models/client.js'),
  Brand = require('../../models/brand.js'),
  Task = require('../../models/task.js'),
  Invites = require('../../models/invite.js'),
  Promotion = require('../../models/promotion.js'),
  Product = require('../../models/product.js'),
  SocialConnector = require('../../models/socialconnector.js'),
  Q = require('q'),
  passport = require('passport'),
  path = require('path'),
  crypto = require('crypto'),
  bucket = "mkupclubapp",
  awsKey = "AKIAIL6CLVQQMQ5GSXFA",
  secret = "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",
  localStrategy = require('../../libs/passport.js')(passport),
  defaultPlan = "54de682d69383f98c697c126",
  jade = require('jade'),
  juice = require('juice2');


var joinFromFacebook = function(req, res) {

  req.checkBody('name').notEmpty();
  req.checkBody('email').isEmail();
  //req.checkBody('location').notEmpty();
  //req.checkBody('phone').notEmpty();
  req.checkBody('network.name').isAlpha();
  req.checkBody('network.token').notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    res.json(400, {
      errors: errors
    });
  } else {
    var data = {
      name: req.body.name,
      email: req.body.email,
      phone: req.body.phone,
      location: req.body.location,
      want_be_resseler: req.body.want_be_resseler
    }
    var promise;
    if (req.account) {
      return Account.updateQ({
          _id: req.account
        }, data)
        .then(function(account) {
          res.json(200, data);
        })
        .catch(function(err) {
          res.json(400, {
            error: err
          });
        });
    } else {
      return SocialConnector.connect(req.body.network)
        .then(function(social) {
          data[req.body.network.name] = social;
          return Account.createQ(data);
        })
        .then(function(account) {
          res.json(200, account);
        })
        .catch(function(err) {
          res.json(400, {
            error: err
          });
        });
    }
  }
};

var joinFromSite = function(req, res) {
  var days = req.body.testdays || 7;

  req.checkBody('name').notEmpty();
  req.checkBody('email').isEmail();
  req.checkBody('phone').notEmpty();
  req.checkBody('password').notEmpty();
  req.checkBody('password_confirmation').notEmpty();
  //req.checkBody('brands').notEmpty();

  var errors = req.validationErrors() || [];
  if (req.body.password_confirmation != req.body.password) {
    errors.push({
      param: 'senha',
      msg: 'Senhas não conferem.'
    });
  }
  if (errors && errors.length) {
    res.format({
      json: function() {
        res.json(400, {
          errors: errors
        });
      },
      html: function() {
        res.render('account/register', {
          account: req.body,
          errors: errors,
          req: req.body,
          days: days
        });
      },
    });
  } else {
    var testdays = days;
    var account = new Account({
      name: req.body.name,
      email: req.body.email,
      phone: req.body.phone,
      validUntil: new Date(new Date().getTime() + testdays * 24 * 60 * 60 * 1000),
      plan: defaultPlan,
      brands: req.body.brands
    });
    account.password = account.generateHash(req.body.password);
    account.saveQ()
      .then(function(account) {
        req.login(account, function() {
          res.format({
            json: function() {
              res.json(200, account);
            },
            html: function() {
              res.redirect('/account/welcome');
            },
          });
        });
        var options = {
          from: 'Luana [MakeUpClub]<luana@makeupclubapp.com>',
          to: account.email,
          subject: 'Bem-vinda ao clube das melhores Consultoras do Brasil!',
          account: account
        };
        //mailer.send('mailer/accounts/welcome', options);
      })
      .catch(function(err) {
        var errors = [{
          param: 'email',
          msg: 'Email em uso'
        }];
        res.format({
          json: function() {
            res.json(400, {
              errors: errors
            });
          },
          html: function() {
            res.render('account/register', {
              account: req.body,
              errors: errors,
              req: req.body,
              days: days
            });
          },
        });
      });
  }
};

var join = function(req, res) {
  if (req.body.network && req.body.network.name == 'facebook') {
    joinFromFacebook(req, res);
  } else {
    joinFromSite(req, res);
  }
};

var welcome = function(req, res) {
  res.render('account/welcome');
}

var me = function(req, res) {
  return Account.findOne({
      _id: req.account
    })
    .populate('plan', 'name price')
    .execQ()
    .then(function(account) {
      var beginOfMonth = new Date();
      beginOfMonth.setDate(1);
      beginOfMonth.setHours(0);
      beginOfMonth.setMinutes(0);
      beginOfMonth.setSeconds(0);
      beginOfMonth.setMilliseconds(0);

      var endOfMonth = new Date(beginOfMonth);
      endOfMonth.setMonth(endOfMonth.getMonth()+1);
      endOfMonth.setMilliseconds(endOfMonth.getMilliseconds()-1);
      
      var promises = [
        account.clients().countQ(),
        account.tasks().where({is_realized: false}).countQ(),
        account.sales().where({date_sale: {$gte: beginOfMonth}}).countQ(),
        account.sales().where({date_sale: {$gte: beginOfMonth, $lte: endOfMonth}, is_realized: false}).countQ(),
        account.salesByDay(beginOfMonth, endOfMonth),
        Invites.findOne({email: req.account.email, is_managed: true}).populate('account').execQ()
      ];
      return Q.all(promises)
      .then(function(result){
        res.format({
          html: function() {
            res.render('account/index', {
              account: account,
              title: 'Bem vinda(o) ao MakeUP Club!',
              join: req.query.join,
              total_clients: result[0],
              total_tasks: result[1],
              total_sales: result[2],
              total_possales: result[3],
              sales_report: result[4],
              supervisor_invite: result[5]
            });
          },
          json: function() {
            res.json(200, account);
          }
        });
      })

    });
};

var newAccount = function(req, res) {
  Brand.where({})
  .execQ()
  .then(function(brands) {
    res.render('account/register.jade', {
      title: 'Crie sua conta',
      req: {},
      days: req.query.testdays || 7,
      brands: brands 
    });
  })
};

var myCoupon = function(req, res) {
  if (req.account) {
    Promotion
      .find({
        _id: req.params.id
      })
      .populate('account', 'name address thumb phone')
      .execQ()
      .then(function(coupons) {
        var coupon = coupons[0];
        for (var j in coupon.coupons) {
          if (coupon.coupons[j].account == req.account.toString()) {
            coupons = [coupon.coupons[j]];
            break;
          }
        }

        if (coupon.account._id != req.account.toString() && (coupon.coupons.length != 1 || coupon.coupons[0].account != req.account.toString())) {
          res.json(404);
        } else {
          res.json(200, coupon);
        }
      })
      .catch(function(error) {
        res.json(500, {
          error: error.toString()
        });
      });
  } else {
    res.json(400, {
      error: "request has no account"
    });
  }
};

var myCoupons = function(req, res) {
  if (req.account) {
    var coupons_admins, coupons_clients;
    return Q.all([
        Promotion.find({
          account: req.account
        })
        .populate('coupons.account', 'name address thumb phone email')
        .populate('account', 'name address thumb phone')
        .execQ(),
        Promotion.find({
          coupons: {
            $elemMatch: {
              account: req.account
            }
          }
        })
        .populate('account', 'name address thumb phone')
        .execQ()
      ])
      .then(function(coupons) {
        coupons_admins = coupons[0];
        coupons_clients = coupons[1];
        for (var i in coupons_clients) {
          var coupons;
          for (var j in coupons_clients[i].coupons) {
            if (coupons_clients[i].coupons[j].account == req.account.toString()) {
              coupons = [coupons_clients[i].coupons[j]];
              break;
            }
          }
          coupons_clients[i].coupons = coupons;
        }
        var my_coupons = coupons_admins.concat(coupons_clients);
        my_coupons.sort(function(a, b) {
          return b.date - a.date;
        });
        res.json(200, my_coupons);
      })
      .catch(function(error) {
        res.json(400, {
          error: error.toString()
        });
      });
  } else {
    res.json(400, {
      error: "request has no account"
    });
  }
};

var addDevice = function(req, res) {
  if (req.account) {
    req.checkBody('device').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
      res.json(400, {
        errors: errors
      });
    } else {
      Account.findOne({
          _id: req.account
        }).execQ().then(function(account) {
          if (account.devices.indexOf(req.body.device) >= 0) {
            res.json(200, account);
          } else {
            account.devices.push(req.body.device);
            account.saveQ()
              .then(function(account) {
                res.json(200, account);
              })
              .catch(function(err) {
                res.json(400, err);
              });
          }
          if (account.devices.length==1) {
            var options = {
              from: 'Luana [MakeUpClub]<luana@makeupclubapp.com>',
              to: account.email,
              subject: 'Benefício exclusivo pra você vender mais (conforme prometido)!',
              account: account
            };
            //mailer.send('mailer/accounts/benefit', options);
            var options2 = {
              from: 'Luana [MakeUpClub]<luana@makeupclubapp.com>',
              to: account.email,
              subject: 'Agora é começar a usar e mudar sua vida pra melhor!',
              account: account
            };
            //mailer.send('mailer/accounts/starttouse', options2);
          }
        })
        .catch(function(err) {
          res.json(400, err);
        });
    }
  } else {
    res.json(400, {
      error: "request has no account"
    });
  }
};

var getLastChanges = function(req, res) {
  var sinceAt = new Date(req.query.sinceAt || new Date());
  var result = {
    clients: {
      deleted: [],
      updated: []
    },
    tasks: {
      deleted: [],
      updated: []
    },
    sales: {
      deleted: [],
      updated: []
    },
    products: {
      updated: []
    }
  };
  var updated_at_where = {
    account: req.account,
    updatedAt: {
      $gt: sinceAt
    },
    deletedAt: {
      $exists: false
    }
  };
  var deleted_at_where = {
    account: req.account,
    deletedAt: {
      $gt: sinceAt
    }
  };
  var brands = req.account.brands.map(function(brand) {
    return brand.toString();
  })
  var updated_products = {
    brand: {
      $in: brands
    },
    updatedAt: {
      $gt: sinceAt
    },
    deletedAt: {
      $exists: false
    }
  };

  var deleted_products = {
    brand: {
      $in: brands
    },
    deletedAt: {
      $gt: sinceAt
    }
  };

  var promises = [
    Sale.where(updated_at_where).populate('items.product').execQ(),
    Sale.where(deleted_at_where).execQ(),
    Client.where(updated_at_where).execQ(),
    Client.where(deleted_at_where).execQ(),
    Task.where(updated_at_where).execQ(),
    Task.where(deleted_at_where).execQ(),
    Product.where(updated_products).execQ(),
    Product.where(deleted_products).execQ(),    
  ];
  
  Q.all(promises)
    .then(function(data) {
      result.sales.updated = data[0];
      result.sales.deleted = data[1];

      result.clients.updated = data[2];
      result.clients.deleted = data[3];

      result.tasks.updated = data[4];
      result.tasks.deleted = data[5];

      result.products.updated = data[6];
      result.products.deleted = data[7];

      res.json(200, result);
    })
    .catch(function(err) {
      res.json(400, {
        error: err.toString()
      });
    })
}

var getUploadKey = function(req, res) {

  var fileName = req.query.fileName,
    expiration = new Date(new Date().getTime() + 1000 * 60 * 10).toISOString(); // expire in 10 minutes

  var policy = {
    "expiration": expiration,
    "conditions": [{
        "bucket": bucket
      },
      ["eq", "$key", fileName], {
        "acl": 'public-read'
      },
      ["starts-with", "$Content-Type", ""],
      ["content-length-range", 0, 524288000]
    ]
  };

  policyBase64 = new Buffer(JSON.stringify(policy), 'utf8').toString('base64');
  signature = crypto.createHmac('sha1', secret).update(policyBase64).digest('base64');
  res.json({
    bucket: bucket,
    awsKey: awsKey,
    policy: policyBase64,
    signature: signature
  });
};

var preview = function(req, res) {
  var view = jade.compileFile(__dirname + '/../../views/' + req.query.view + '.jade', {});
  var body = view({
    account: req.account,
    invite: {name:'Fulana', email: 'invite@example.com'}
  });
  juice.juiceContent(body, {
    url: 'file://'
  }, function(err, html) {
    if (err) {
      res.send(err);
    } else { 
      res.send(html);
    }

  });
};

module.exports = function(router) {
  router.get('/', passport.ensureLogged, me);
  router.get('/new', newAccount);
  router.get('/welcome', passport.ensureLogged, welcome);

  router.get('/coupons', myCoupons);
  router.get('/coupons/:id', myCoupon);
  router.post('/', join);
  router.post('/device', addDevice);
  router.get('/lastChanges', passport.ensureLogged, getLastChanges);
  router.get('/photo/getuploadkey', getUploadKey);

  router.get('/preview', passport.ensureLogged, preview);

  return {
    me: me,
    join: join,
  };
};
