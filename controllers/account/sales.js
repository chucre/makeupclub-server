var mailer = require('../../models/mailer.js'),
    Account = require('../../models/account.js')(mailer),
    Client = require('../../models/client.js'),
    Q = require('q'),
    passport = require('passport'),
    localStrategy = require('../../libs/passport.js')(passport);

var index = function(req, res) {
  var end = new Date();
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  end.setMilliseconds(999);
  var start = new Date(end);
  start.setMonth(start.getMonth()-1);
  Q.all([
    req.account.sales()
              .populate('client','id name photo_profile')
              .populate({path:'items.product'}).where({date_sale:{$gte: start, $lte: end}, deletedAt: {$exists: false}}).execQ(),
    req.account.salesByDay(start, end)
  ])

  .then(function(results){
    var numeral = require('numeral');
    numeral.language('pt-br', require('../../libs/numeral_pt-br.js'));
    numeral.language('pt-br');
    
    res.render('account/sales',{
      sales: results[0], 
      sales_report: results[1],
      moment: require('moment'),
      numeral: numeral
    });
  })
}

module.exports = function(router) {  
  router.get('/',  passport.ensureLogged, index);
};
