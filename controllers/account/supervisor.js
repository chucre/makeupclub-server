var passport = require('passport'),
    Invites   = require('../../models/invite.js');


var index = function(req, res){
  try {
    var locals = {};
    req.account.populateQ('supervisedAccounts')
    .then(function(account){
      locals.account = account;
      return Invites.where({account: account, is_managed: true}).execQ();
    })
    .then(function(invites){
      locals.invites = invites;
      res.render('account/supervisor/index',locals); 
    })
    .catch(function(err){
      res.send(err.toString());
    })
  } catch(err) {
    res.send(err.toString());
  }
}

var clients = function(req, res){
	res.render('account/supervisor/clients');
}

var sales = function(req, res){
	res.render('account/supervisor/sales');
}

var demonstrations = function(req, res){
	res.render('account/supervisor/demonstrations');
}

var opportunities = function(req, res){
	res.render('account/supervisor/opportunities');
}



module.exports = function(router) {  
  router.get('/', passport.ensureLogged, index);
  router.get('/clients', passport.ensureLogged, clients);
  router.get('/sales', passport.ensureLogged, sales);
  router.get('/demonstrations', passport.ensureLogged, demonstrations);
  router.get('/opportunities', passport.ensureLogged, opportunities);
};

