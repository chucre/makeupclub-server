var mailer = require('../../models/mailer.js'),
    passport = require('passport'),
    Account = require('../../models/account.js')(mailer),
    Invite = require('../../models/invite.js'),
    Q = require('q');

var accept = function(req, res) {
  Invite.findOne({_id: req.body.id, is_managed: true, email: req.account.email})
  .populate('account')
  .execQ()
  .then(function(invite){
    if (invite) {
      req.account.supervisor = invite.account;
      req.account.save();
      invite.account.supervisedAccounts = invite.account.supervisedAccounts || [];
      if (invite.account.supervisedAccounts.indexOf(req.account)==-1) {
        invite.account.supervisedAccounts.push(req.account);
        invite.account.save();
      }
      Invite.removeQ({is_managed: true, email: req.account.email});
    }
  })
  .catch(function(err){
    console.log(err.toString());
  })
  .finally(function(){
    res.redirect('/account');
  })
}

var invite = function(req, res) {
  var promises = [];
  for(var i in req.body.contacts) {
    var invite = new Invite(req.body.contacts[i]);
    invite.is_managed = true;
    invite.account = req.account;
    promises.push(invite.saveQ())
  }
  Q.all(promises)
  .then(function(invites){
    path = '/account/supervisor';
    res.format({
      json: function() {
        res.json(200, req.account);
      },
      html: function() {
        res.redirect(path);
      },
    });
    for(var i in invites){
      var invite = invites[i];
      var options = {
        from: 'Luana Caetano [MakeupClub App]<luana@makeupclubapp.com>',
        to: invite.email,
        subject: '',
        account: req.account,
        invite: invite,
        subject: req.account.name+' lhe enviou um convite.'
      };
      mailer.send('mailer/accounts/invite', options);
    }
  })
  .catch(function(err){
    res.render('account/',{errors: [err]});
  });
};

module.exports = function(router) {
  router.post('/', passport.ensureLogged, invite);
  router.post('/accept', passport.ensureLogged, accept);
};
