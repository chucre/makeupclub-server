var mailer = require('../../models/mailer.js'),
    Account = require('../../models/account.js')(mailer),
    passport = require('passport'),
    localStrategy = require('../../libs/passport.js')(passport);

var passwordForgot = function(req, res) {
  res.render('account/passwordForgot');
}

var newPassword = function(req, res) {
  res.render('account/passwordSetNew', {
    params: req.query
  });
}

var resetPassword = function(req, res) {
  Account.findOne({
      email: req.body.email
    })
    .execQ()
    .then(function(account) {
      if (account) {
        account.generateResetHash();
        account.saveQ()
          .then(function() {
            view = 'mailer/reset_password',
              options = {
                from: 'MakeUP Club App<mirla@makeupclubapp.com>',
                to: account.email,
                subject: 'Troca de senha',
                account: account
              };
            mailer.send(view, options);
          })
      }
    })
    .finally(function() {
      res.render('account/passwordTokenSent');
    })
}

var setPassword = function(req, res) {
  var token_reset = req.query.token_reset || 'semtokendefinido';
  Account
    .findOne({
      password_reset_token: token_reset
    })
    .execQ()
    .then(function(account) {
      console.log(req.query);
      console.log(account);
      console.log(token_reset);
      if (account && req.body.password && req.body.password == req.body.password_confirmation) {
        account.password = account.generateHash(req.body.password);
        account.password_reset_token = null;
        account.saveQ()
          .then(function() {
            req.login(account, function() {
              res.redirect('/painel/#/login');
            });
          })
      } else {
        res.render('account/passwordSetNew', {
          errors: ["Token ou senhas invalidas."]
        });
      }
    })
    .catch(function(err) {
      console.log(err);
      res.render('account/passwordSetNew', {
        errors: [err]
      });
    })
}

module.exports = function(router) {  
  router.get('/forgot', passwordForgot);
  router.post('/reset', resetPassword);
  router.get('/new', newPassword);
  router.post('/new', setPassword)
};
