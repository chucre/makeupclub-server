var mailer = require('../../models/mailer.js'),
    Account = require('../../models/account.js')(mailer),
    Client = require('../../models/client.js'),
    passport = require('passport'),
    localStrategy = require('../../libs/passport.js')(passport),
    Q = require('q');

var index = function(req, res) {
  var end = new Date();
  var start = new Date(end);
  start.setMonth(start.getMonth()-1);
  Q.all([
    req.account.clients().sort('name').execQ(),
    req.account.clientsByDay(start, end)
  ])

  .then(function(results){
    res.render('account/clients',{clients: results[0], clients_report: results[1]});
  });
};

module.exports = function(router) {  
  router.get('/', passport.ensureLogged, index);
};
