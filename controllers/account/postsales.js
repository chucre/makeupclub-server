var mailer = require('../../models/mailer.js'),
    Account = require('../../models/account.js')(mailer),
    Client = require('../../models/client.js'),
    passport = require('passport'),
    localStrategy = require('../../libs/passport.js')(passport);

var index = function(req, res) {
  res.render('account/postsales');
};

module.exports = function(router) {  
  router.get('/', index);
};
