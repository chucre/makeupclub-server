var Brand = require('../models/brand.js');

var brands = function(req, res) {
  return Brand.find().execQ()
  .then(function(brands){
    res.json(200,brands)
  })
}

module.exports = function (router) {
  router.get('/', brands);

  return {
    brands: brands
  }
};
