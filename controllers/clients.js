var Client = require('../models/client.js');

var createClient = function(req, res) {

  if (!req.account) {
    res.json(400, {
      error: 'invalid credentials'
    });
  } else {
    req.checkBody('name').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
      res.json(400, {
        errors: errors
      });
    } else {
      var client = new Client(req.body);
      client.account = req.account;

      client.saveQ().then(function(client) {
          res.json(200, client);
        })
        .catch(function(error) {
          res.json(400, {
            error: error.toString()
          });
        });
    }
  }
};

var updateClient = function(req, res) {
  if (!req.account) {
    res.json(400, {
      error: 'invalid credentials'
    });
  } else {
    req.checkBody('name').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
      res.json(400, {
        errors: errors
      });
    } else {
      var id = req.params.id;
      delete req.body._id;
      Client.findOne({
        _id: id,
        account: req.account
      })
      .execQ()
      .then(function(client) {
        client.set(req.body);
        return client.saveQ()
        .then(function(){
          res.json(200, client);
        })
      })
      .catch(function(error) {
        res.json(400, {
          error: error.toString()
        });
      });
    }
  }
};

var removeClient = function(req, res) {
  if (!req.account) {
    res.json(400, {
      error: 'invalid credentials'
    });
  } else {
    Client.findOne({_id: req.params.id})
    .execQ()
    .then(function(client) {
      if (client) {
        client.paranoid_remove();
        return client.saveQ()
        .then(function(){
          res.json(200, {});
        });
      } else {
        res.json(404, {});
      }
    })
    .catch(function(error) {
      res.json(400, {
        error: error.toString()
      });
    });
  }
};

module.exports = function(router) {
  router.post('/:id', updateClient);
  router.post('/', createClient);
  router.delete('/:id', removeClient);
};
