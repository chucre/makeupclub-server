var Product = require('../models/product.js'),
    Brand = require('../models/brand.js'),
    passport = require('passport'),
    localStrategy = require('../libs/passport.js')(passport),
    Q = require('q');

var products = function(req, res) {
  var brand = req.query.brand || new Brand();
  var page_size = req.query.page_size || 80;
  var page = req.query.page || 1;
  Q.all([
    Product
      .find({
        brand: brand
      })
      .sort('name')
      .execQ(),
    Brand.find({}).execQ()
  ])
  .then(function(results) {
    res.format({
      json: function() {
        res.json(200, results[0]);
      },
      html: function() {
        res.render('products/index', {
          products: results[0],
          brands: results[1],
          brand: brand
        });
      },
    });
  })
  .catch(function(err){
    console.log(err.toString());
    res.redirect('/products/');
  });
};

var addProduct = function(req, res) {
  
  var product = new Product(req.body);

  product.saveQ().then(function(product) {
    res.redirect('/products?brand='+req.body.brand);
  })
  .catch(function(error) {
    res.json(400, {
      error: error.toString()
    });
  });
};

var updateProduct = function(req, res) {
  
  Product.findOne({_id: req.params.id})
  .execQ()
  .then(function(product){
    product.set(req.body)
    .saveQ()
    .then(function(){
      res.redirect('/products?brand='+req.body.brand);
    })
  })

};


module.exports = function(router) {
  router.get('/', passport.ensureLogged, products);
  router.post('/', passport.ensureLogged, passport.isPermitedTo('create', 'products'), addProduct);
  router.post('/:id', passport.ensureLogged, passport.isPermitedTo('update', 'products'), updateProduct);

};
