var LocalStrategy = require('passport-local').Strategy,
    mailer = require('../models/mailer.js'),
    Account = require('../models/account.js')(mailer);


module.exports = function(passport) {
  passport.serializeUser(function(account, done) {
    done(null, account._id);
  });

  passport.deserializeUser(function(id, done) {
    Account.findOne({_id: id})
    .execQ()
    .then(function(account){
      done(null, account);
    })
    .catch(function(err){
      done(err,null);
    })
  });

  passport.use(new LocalStrategy(
    function(username, password, done) {
      Account.findOne({ email: username })
      .execQ()
      .then(function(account){
        if (account.validPassword(password)) {
          return done(null, account);
        } else {
          return done(null, false, { message: 'Incorrect password or login.' });          
        }
      })
      .catch(function(err){
        return done(null, false, { message: 'Incorrect password or login.' });
      });
    }
  ));

  var isPermitedTo = function(action, resource) {
    return (this.permissions && this.permissions[resource] && this.permissions[resource].indexOf(action) >= 0);
  }

  passport.isPermitedTo = function(action, resource) {
    return function(req, res, next) {
      if (req.account) {
        Account.findOne({_id: req.account._id || req.account})
        .execQ()
        .then(function(account){
          if (isPermitedTo.call(account, action, resource)) {
            next();
          } else {
            res.redirect('/account');
          };
        })
        .catch(function(e){
          res.redirect('/account');
        })
      } else {
        res.redirect('/account');
      }
    }
  }


  passport.ensureLogged = function(req, res, next) {
    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
      req.account = req.user;
      res.locals.account = req.user;
      res.locals.isPermitedTo = function(action, resource){
        return isPermitedTo.call(req.account, action, resource);
      }
      return next();
    } else if (req.account) {
      Account.findOne({_id: req.account})
      .execQ()
      .then(function(account){
        req.account = account;
        res.locals.account = req.account;
        next();
      })
      .catch(function(err) {
        console.log(err.toString());
      })
    } else {
      res.redirect('/session/new');
    }
  }

}
