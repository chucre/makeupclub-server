var Q = require('q'),
    Plan = require('../models/plan.js'),
    rest = require('restler'),
    email = 'fernandochucre@gmail.com',
    qs = require('qs'),
    token = '9ABE8FD46D9A4311B0536718C3E082E6',
    // token = "5FD7692C337A4019B68F59408176FE1F",
    pagseguro_paths;

pagseguro_paths = {
  production: {
    'CREATE_PREAPROVALS': 'https://ws.pagseguro.uol.com.br/v2/pre-approvals/request',
    'PROCESS_APPROVALS': 'https://pagseguro.uol.com.br/v2/pre-approvals/request.html',
    'REQUEST_PREAPPROVAL_BY_TRANSACTION': 'https://ws.pagseguro.uol.com.br/v2/pre-approvals/notifications',
  }, 
  sandbox: {
    'CREATE_PREAPROVALS': 'https://ws.sandbox.pagseguro.uol.com.br/v2/pre-approvals/request',
    'PROCESS_APPROVALS': 'https://sandbox.pagseguro.uol.com.br/v2/pre-approvals/request.html',
    'REQUEST_PREAPPROVAL_BY_TRANSACTION': 'https://ws.sandbox.pagseguro.uol.com.br/v2/pre-approvals/notifications',
  }
};

var getPaths =  function() {
  var env = process.env.PAGSEGURO_ENV || 'sandbox';
  return pagseguro_paths[env];
};

var generatePlan = function(account, plan) {
  var def = Q.defer();
  var total_amount_plan = Math.ceil(plan.price/Plan.periodicity[plan.periodicity].valid_base_in_month*24);
  var endPlan = new Date();
  endPlan.setYear(endPlan.getFullYear()+2);
  var formatedEndDate = JSON.stringify(endPlan);
  var requestData = {
    email: email,
    token: token,
    reference: account._id.toString(),
    preApprovalCharge: 'auto',
    preApprovalName: plan.name,
    preApprovalDetails: plan.detail,
    preApprovalAmountPerPayment: plan.price.toFixed(2),
    preApprovalPeriod: Plan.periodicity[plan.periodicity].pagseguro_value,
    preApprovalFinalDate: formatedEndDate,
    preApprovalMaxTotalAmount: total_amount_plan.toFixed(2),
    receiverEmail: email,
    redirectURL: 'http://www.makeupclubapp.com/account'
  };

  var args = {
    data: requestData,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    encoding: 'UTF-8'
  };

  rest.post(getPaths().CREATE_PREAPROVALS, args)
  .on('complete',function(data){
    if (data && !(data instanceof Error) && data.preApprovalRequest && data.preApprovalRequest.code) {
      def.resolve(data.preApprovalRequest);
    } else {
      def.reject(data);
    }
  })
  .on('error', function(err){
    def.reject(error); 
  });

  return def.promise;
};


var getTransaction = function(notificationType, notificationCode) {
  var def = Q.defer();
  var requestData = {
    email: email,
    token: token
  };

  var url = getPaths().REQUEST_PREAPPROVAL_BY_TRANSACTION+'/'+notificationCode+'?'+qs.stringify(requestData);
  console.log(url);
  rest.get(url)
  .on('complete',function(data){
    if (data && !(data instanceof Error) && data.preApproval) {
      def.resolve(data.preApproval);
    } else {
      def.reject(data);
    }
  })
  .on('error', function(err){
    def.reject(error); 
  });

  return def.promise;
};

module.exports  = {
  generatePlan: generatePlan,
  getPaths: getPaths,
  getTransaction: getTransaction
};