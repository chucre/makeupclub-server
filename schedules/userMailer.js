'use strict';
var mailer  = require('../models/mailer.js'), 
    Account = require('../models/account.js')(mailer),
    Sales   = require('../models/sale.js'),
    Q       = require('q');
    
var getRangeDay = function(date, inDays) {
  var start = new Date(date.getTime()+(inDays * 24 * 60 * 60 * 1000));
  start.setHours(0);
  start.setMinutes(0);
  start.setSeconds(0);
  start.setMilliseconds(0);

  var end = new Date(start);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  end.setMilliseconds(999);
  return {
    start: start,
    end: end
  };
};

var sendEmail = function(inDays, view, subject, conditions) {
  return function(job, done) {
    var range = getRangeDay(new Date(), inDays);
    Account.where({updatedAt: {$gte: range.start, $lte: range.end}})
    .where(conditions)
    .execQ()
    .then(function(accounts){
      accounts.forEach(function(account){
        var options = {
          from: 'Luana [MakeupClub App]<luana@makeupclubapp.com>',
          to: account.email,
          subject: subject,
          account: account
        };
        mailer.send(view,options);
      });
    })
    .catch(function(err){
      console.log(err);
    })
    .finally(done);
  };
};

var fixAccounts = function(job, done) {
  Account.where({password: {$exists: true}, createdAt: {$exists: false}})
  .execQ()
  .then(function(accounts){
    var result = Q(0);
    accounts.forEach(function(account){
      var options = {
        from: 'Luana Caetano <luana@makeupclubapp.com>',
        to: account.email,
        subject: '[MakeUpClub] - Obrigada...',
        account: account
      };
      result = result.then(function(){
        account.createdAt = new Date();
        account.save();
        return mailer.send('mailer/accounts/welcome', options);
      });
    });
  })
  .catch(function(err){
    console.log(err);
  })
  .finally(done);
}

var sendEmailWithoutSales = function(inDays, view, subject) {
  return function(job, done) {
    Sales.distinct('account')
    .execQ()
    .then(function(accounts_with_sales){
      var range = getRangeDay(new Date(), inDays);
      Account.where({updatedAt: {$gte: range.start, $lte: range.end}})
      .where({'devices.0':{$exists: true}, '_id':{$nin: accounts_with_sales}})
      .execQ()
      .then(function(accounts){
        var result = Q(0);
        accounts.forEach(function(account){
          var options = {
            from: 'Luana [MakeupClub App]<luana@makeupclubapp.com>',
            to: account.email,
            subject: subject,
            account: account
          };
          result = result.then(function(){
            return mailer.send(view,options);
          });
        });
      })
      .catch(function(err){
        console.log(err);
      })
      .finally(done);
    })
  }
}


module.exports = function(agenda) {
  /*
  agenda.define('fix accounts', fixAccounts);
  agenda.every('0 11 * * *', 'fix accounts')

  agenda.define('no download #01', sendEmail(-2, 'mailer/accounts/nodownload_01' ,'Você vai perder essa oportunidade?', {'devices.0':{$exists: false}}));
  agenda.every('0 12 * * *', 'no download #01');

  agenda.define('no download #02', sendEmail(-4, 'mailer/accounts/nodownload_02' ,'Estou muito trite...', {'devices.0':{$exists: false}}));
  agenda.every('0 12 * * *', 'no download #02');

  agenda.define('no use #01', sendEmailWithoutSales(-2, 'mailer/accounts/nouse_01' ,'Quem usa já está vendendo mais!'));
  agenda.every('0 12 * * *', 'no use #01');

  agenda.define('no use #02', sendEmailWithoutSales(-4, 'mailer/accounts/nouse_02' ,'Sinto dizer, mas você está perdendo vendas...'));
  agenda.every('0 12 * * *', 'no use #02');
  */
};
