'use strict';
var mailer  = require('../models/mailer.js'), 
    Account = require('../models/account.js')(mailer),
    Sales   = require('../models/sale.js'),
    Tasks   = require('../models/task.js'),
    Product = require('../models/product.js'),
    push_notification = require('../models/push_notification.js'),
    superagent = require('superagent'),
    Q       = require('q'),
    fs = require('fs'),
    request = require('request'),
    shell = require('shelljs');

var download = function(uri, filename, callback){
  var r = request(uri).pipe(fs.createWriteStream(filename));
  r.on('close', callback);
};

var withoutSales = function(job, done) {
  var startAt = new Date();
  var since = new Date(startAt.getTime()-5 * 24 * 60 * 60 * 1000);

  Sales.distinctQ('account', {date_sale: {$gte: since}, deletedAt:{$exists: false}})
  .then(function(accounts_with_sales){
    return Account.where({
      '_id': {$nin: accounts_with_sales},
      'devices.0': {$exists: true}
    })
    .or([{
      'mailer.without_sales_warning': {$lt: since},
    },{
      'mailer.without_sales_warning': {$exists: false},
    }])
    .execQ();
  })
  .then(function(accounts){
    accounts.forEach(function(account){
      push_notification('now','5 dias sem vendas???', {}, account.devices)
      account.mailer = account.mailer || {};
      account.mailer.without_sales_warning = startAt;
      account.save();
    });
  })
  .catch(function(err){
    console.log(err);
  })
  .finally(done);
}



var withoutTasks = function(job, done) {
  var startAt = new Date();
  var until = new Date(startAt.getTime()+3 * 24 * 60 * 60 * 1000);
  var lastWarning = new Date(startAt.getTime()-3 * 24 * 60 * 60 * 1000);

  Tasks.distinctQ('account', {start_at: {$gte: startAt, $lt: until}, deletedAt:{$exists: false}})
  .then(function(accounts_with_tasks){
    return Account.where({
      '_id': {$nin: accounts_with_tasks},
      'devices.0': {$exists: true}
    })
    .or([{
      'mailer.without_tasks_warning': {$lt: lastWarning},
    },{
      'mailer.without_tasks_warning': {$exists: false},
    }])
    .execQ();
  })
  .then(function(accounts){
    accounts.forEach(function(account){
      push_notification('now','Nada nos proximos 3 dias??', {}, account.devices)
      account.mailer = account.mailer || {};
      account.mailer.without_tasks_warning = startAt;
      account.save();
    });
  })
  .catch(function(err){
    console.log(err);
  })
  .finally(done);
}

var saveProduct = function(product_original, brand, url) {
  var product = null;
  //{"id":362,"category":{"id":2,"name":"Maquiagem","description":"Aqui você encontra todas as linhas de maquiagens Mary Kay."},"active":false,"modified":1437484235000}
  console.log('finding product:'+ product_original.cod);
  return Product.findOneQ({
    brand: brand._id,
    code: product_original.cod.trim()
  })
  .then(function(product){
    if (!product) {
      console.log('savind new product:'+ product_original.cod);
      product = new Product({
        "code": product_original.cod.trim(),
        "name":product_original.name,
        "description":product_original.description,
        "benefits":product_original.benefits,
        "price": product_original.price,
        "original_price":product_original.price,
        "line": product_original.category.name,
        "catalog_name": brand.name,
        "brand": brand._id
      })
      return product.saveQ()
      .then(function(_product){
        console.log('product created:'+ _product._id);
        var file = './public/images/product/'+_product._id;
        if (url) {
          console.log('add to download:'+url);
          fs.writeFileSync('/tmp/products_download.txt', _product._id+":"+url+"\n", {flag: 'a'});

          // console.log('trying download:'+url);
          // shell.exec('curl '+url+' --output '+file);
          // shell.exec('convert '+file+' -fuzz 1% -trim +repage '+file);
          // shell.exec('convert '+file+' -resize "350x350" -gravity center -background white -extent 350x350 '+file);
        }
      })
      .catch(function(err) {
        console.log(err.toString());
      });
    } else {
      console.log('product updated:'+ product._id);
      var change = false;
      if (product_original.price!=product.price) {
        product.price = product_original.price;
        product.original_price = product_original.original_price;
        change = true;
      }
      if (product_original.description!=product.description) { 
        product.description = product_original.description;
        change = true;
      }
      if (product_original.name!=product.name) { 
        product.name = product_original.name;
        change = true;
      }
      var file = './public/images/product/'+product._id;
      if (url && !shell.test('-f', file)) {
        console.log('add to download:'+url);
        fs.writeFileSync('/tmp/products_download.txt', product._id+":"+url+"\n", {flag: 'a'});
      }
      if (change) {
        console.log('savind product:'+ product._id);
        return product.saveQ();
      } else {
        console.log('not savind product:'+ product._id);
        return Q.when(0);
      }
    }
  })
  .catch(function(err){
    console.log(err.toString());
  })
}

var marykayCrawler = function(job, done) {
  superagent.get('http://54.69.225.196:8080/marykay/api/products?date=01/07/2010%2015:56:00')
  .end(function(err, res){
    if (!err) {
      var brand  = { _id: '551dbadf454ba2c1310fb5dc', name: 'MaryKay' }

      for (var i in res.body) {
        var url = 'http://54.69.225.196:8080/marykay/api/products/'+res.body[i].id+'/photo';
        saveProduct(res.body[i], brand, url);
      }
      done();
    } else {
      done();
    }
  })
}

var pravenderCrawler = function(job, done) {
  if (!shell.test('-f', '/tmp/pravender.json')) {
    return done();
  }
  var data = JSON.parse(fs.readFileSync('/tmp/pravender.json', 'utf8'));
  var products = {};
  var productsCode = {};
  var suppliers = {};
  var lines = {};

  var brands = { 
    '1288029493428019270': { _id: '551dbadf454ba2c1310fb5e2', name: 'Avon' },
    '1288029493428019267': { _id: '551dbadf454ba2c1310fb5dd', name: 'Natura' },
    '1288029493428454758': { _id: '56040013b7d0d5056851712f', name: 'Demillus' },
    '1288029493428019264': { _id: '551dbadf454ba2c1310fb5de', name: 'O Boticario' },
    '1288029493428441730': { _id: '56040037b7d0d50568517130', name: 'Herbalife' },
    '1288029493428019266': { _id: '551dbadf454ba2c1310fb5e0', name: 'Eudora' },
    '1288029493428019269': { _id: '551dbadf454ba2c1310fb5e1', name: 'Jequiti' },
    '1288029493428019268': { _id: '551dbadf454ba2c1310fb5dc', name: 'MaryKay' },
    '1288029493428019265': { _id: '5604067dc0d39da982bfea9e', name: 'Tupperware' }
  }

  var getProduct = function(id) {
    products[id] = products[id]||{id: id};
    return products[id];
  }

  var getLine = function(id) {
    lines[id] = lines[id]||{id: id};
    return lines[id];
  }

  var getSupplier = function(id) {
    suppliers[id] = suppliers[id]||{id: id};
    return suppliers[id];
  }

  for(var i in data.tables) {
    var table = data.tables[i];
    var table_name = table.n.toLowerCase();
    console.log(table_name);
    switch(table_name) {
      case 'tbl_product':
        for(var j in table.r) {
          var product = getProduct(table.r[j].k);
          product.name = table.r[j].f[5];
          product.supplier = getSupplier(table.r[j].f[0]);
          product.category = getLine(table.r[j].f[2]);
        }
        break;
      case 'tbl_productcode':
        for(var j in table.r) {
          var product = getProduct(table.r[j].f[0]);
          productsCode[table.r[j].k] = table.r[j].f[0];
          product.cod = table.r[j].f[1];
        }
        break;
      case 'tbl_supplier':
        for(var j in table.r) {
          var supplier = getSupplier(table.r[j].k);
          supplier.name = table.r[j].f[3];
        }
        break;
      case 'tbl_pdtdescription':
        for(var j in table.r) {
          var product = getProduct(table.r[j].f[0]);
          product.description = table.r[j].f[1];
        }
        break;
      case 'tbl_productimg':
        for(var j in table.r) {
          var product = getProduct(table.r[j].f[1]);
          product.img = 'http://gerirmosimg.cloudapp.net/pv_images/'+product.supplier.id+"/"+table.r[j].f[3]+'.jpeg';
        }
        break;
      case 'tbl_productprice':
        for(var j in table.r) {
          var product = getProduct(productsCode[table.r[j].f[1]]);
          product.price = table.r[j].f[2].toString().replace(',','.');
        }
        break;
      case 'tbl_pdtline':
        for(var j in table.r) {
          var line = getLine(table.r[j].k);
          line.name = table.r[j].f[1];
        }
        break;
    }
  }
  var nextPromise = function(product, promise) {
    return promise.then(function(){
      console.log('inside promise: '+product.cod);
      return saveProduct(product, brands[product.supplier.id], product.img);
    })
  }
  var promise = Q.when(0);
  for (var i in products) {
    console.log('outside promise: '+products[i].name);
    promise = nextPromise(products[i], promise);
  }
  return done();
}

module.exports = function(agenda) {
  agenda.define('marykay_crawler', marykayCrawler);
  agenda.every('24 hours', 'marykay_crawler');
  
  agenda.define('pravender_crawler', pravenderCrawler);
  agenda.every('24 hours', 'pravender_crawler');
  

  agenda.define('5 dias sem vendas', withoutSales);
  agenda.every('*/5 * * * *', '5 dias sem vendas');

  agenda.define('3 dias sem agendas', withoutTasks);
  agenda.every('*/5 * * * *', '3 dias sem agendas');

  return {
    pravenderCrawler: pravenderCrawler,
    marykayCrawler: marykayCrawler
  }
};
