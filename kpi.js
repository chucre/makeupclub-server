var accounts = [],
    promos = db.promotions.find().toArray(),
    total_coupons = 0;
for(var i in promos) {
  for(var j in promos[i].coupons) {
  	total_coupons++;
  	accounts.push(promos[i].coupons[j].account);
  }
  for(var k in promos[i].schedules) {
  	accounts.push(promos[i].schedules[k])
  }
}

var total_promotions  = db.promotions.count();
var total_accounts = db.accounts.count({apps: 'makeupclub' });
var active_accounts = db.accounts.find({_id: {$in: accounts}}).size();
