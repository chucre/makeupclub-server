# encoding: UTF-8
require "httparty"
 
options = { 
  :request  => {
    :application  => "CCD96-ED0F3",
    :auth => "bGxITHUFy0kiOtlFxbeEBnVVICYW0XVbj0feNz0ewkTqJfI8RzBoWQrpc8zcBODcL1pYsp6u6UZPwl8OFKig",
    :notifications => [{
      :send_date  => "now",
      :platforms => [3],
      :content  => "Não perca tempo, agende agora mesmo sua lavagem",
      :data => {
        :type => 1,
        :carwash => 15,
        :title => ''
      },
      :devices => ['APA91bHAYzSIiq9asA069l1t4K4uybC_ngxeEUahGMuz0vFmuueJxVCvFshL3A9HzOdvDFdBdc_Mu5WstyzrCBfPtvK6aIAJduFVSGchOxbyr0sQlMi1ofUUapqSRIZgyncvzROSIhKcFRgrd6aWtdS4Bei4t5aA4g']
    }]
  }
}
 
response = HTTParty.post("https://cp.pushwoosh.com/json/1.3/createMessage", :body  => options.to_json, :headers => { 'Content-Type' => 'application/json' })
 
puts response.body, response.code, response.message, response.headers.inspect