var gulp = require('gulp'),
    mocha = require('gulp-mocha');

var serverTests = [
  'tests/**/*.js'
]

gulp.task('test:server', function(){
  return gulp.src(serverTests)
    .pipe(mocha({
      reporter: 'spec'
    }))
    .on('error', function(err) {
      throw err;
    });;
});

gulp.task('test', ['test:server']);
